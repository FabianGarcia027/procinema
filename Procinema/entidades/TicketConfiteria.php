<?php
/*
Clase que representa a la entidad TicketConfiteria
*/
class TicketConfiteria  {
//-------------------------
//Atributos
//-------------------------

  /**
  *Representa el numero de identificación del Ticket de confiteria
  * @var [integer]
  */
  private $cod_ticket_confiteria;

  /**
  * Representa el código de identificación de la confiteria 
  * @var [integer]
  */
  private $cod_confiteria;


  /**
  * Representa el precio del Ticket
  * @var [integer]
  */
  private $precio;

//----------------------------
//Constructor
//----------------------------


/**
 * @param [integer]cod_ticket : Número de identificación del ticket de compra
* @param [integer]cod_confiteria : Número de identificación de la confiteria
* @param [integer]precio: Precio del ticket
 */
public function __construct($cod_ticket_confiteria,$cod_confiteria,$precio){

	$this->cod_ticket_confiteria=$cod_ticket_confiteria;
	$this->cod_confiteria=$cod_confiteria;
	$this->precio=$precio;
}

/**
 * Metodo para obtener la identificación del ticket
 * @return [integer]Identificación del ticket
 */
public function getCod_ticket_confiteria(){
	return $this->cod_ticket_confiteria;
}

/**
 * Método que modifica el número de identificación del ticket
 * @param [integer]Numero a modificar
 */
public function setCod_ticket_confiteria($cod_ticket_confiteria){
	$this->cod_ticket_confiteria=$cod_ticket_confiteria;
	return $this;
}

/**
 * Obtener la confiteria
 * @return [String] [Confiteria]
 */
public function getCod_confiteria(){
	return $this->cod_confiteria;
}

/**
 * Modificar la confiteria
 * @param [Void] $cod_confiteria [Nueva confiteria]
 */
public function setCod_confiteria($cod_confiteria){
	$this->cod_confiteria=$cod_confiteria;
	return $this;
}


/**
 * Metodo para obtener la precio del tickets
 * @return [precio] precio del ticket
 */
 function getPrecio(){
	return $this->precio;
}

/**
 * Metodo para modificar la precio del ticket
 * @param [precio] precio a modificar
 */
public function setPrecio($precio){
	$this->precio=$precio;
	return $this;
}




}

?>
