<?php
/*
Clase que representa a la entidad TicketPelicula
*/
class TicketPelicula  {
//-------------------------
//Atributos
//-------------------------

  /**
  *Representa el numero de identificación del Ticket de pelicula
  * @var [integer]
  */
  private $cod_ticket_pelicula;

  /**
  * Representa el código de identificación de la asignacion
  * @var [integer]
  */
  private $cod_asignacion;


  /**
  * Representa el código de identificación del asiento
  * @var [integer]
  */
  private $cod_asiento;


  /**
  * Representa el precio del Ticket
  * @var [integer]
  */
  private $precio;

//----------------------------
//Constructor
//----------------------------


/**
 * @param [integer]cod_ticket : Número de identificación del ticket de compra
* @param [integer]cod_asignacion : Número de identificación de la asignacion
* @param [integer]cod_asiento : Número de identificación del asiento
* @param [integer]precio: Precio del ticket
 */
public function __construct($cod_ticket_pelicula,$cod_asignacion,$cod_asiento,$precio){

	$this->cod_ticket_pelicula=$cod_ticket_pelicula;
	$this->cod_asignacion=$cod_asignacion;
  $this->cod_asiento=$cod_asiento;
	$this->precio=$precio;
}

/**
 * Metodo para obtener la identificación del ticket
 * @return [integer]Identificación del ticket
 */
public function getCod_ticket_pelicula(){
	return $this->cod_ticket_pelicula;
}

/**
 * Método que modifica el número de identificación del ticket
 * @param [integer]Numero a modificar
 */
public function setCod_ticket_pelicula($cod_ticket_pelicula){
  $this->cod_ticket_pelicula=$cod_ticket_pelicula;
}

/**
 * Obtener la asignacion
 * @return [String] [AsignacionPHS]
 */
public function getCod_asignacion(){
	return $this->cod_asignacion;
}

/**
 * Modificar la asignacion
 * @param [Void] $cod_asignacion [Nueva asignacion]
 */
public function setCod_asignacion($cod_asignacion){
	$this->cod_asignacion=$cod_asignacion;
	return $this;
}


/**
 * Obtener el asiento
 * @return [String] [Asiento]
 */
public function getCod_asiento(){
  return $this->cod_asiento;
}

/**
 * Modificar el asiento
 * @param [Void] $cod_asiento [Nuevo asiento]
 */
public function setCod_asiento($cod_asiento){
  $this->cod_asiento=$cod_asiento;
  return $this;
}
/**
 * Metodo para obtener la precio del tickets
 * @return [precio] precio del ticket
 */
 function getPrecio(){
	return $this->precio;
}

/**
 * Metodo para modificar la precio del ticket
 * @param [precio] precio a modificar
 */
public function setPrecio($precio){
	$this->precio=$precio;
	return $this;
}




}

?>
