<?php
/**
 * Clase que representa la entidad Asignación Pelicula-Horario-Sala
 */
class AsignacionPHS{


/**
 * código de la asignacion
 * @var [integer]
 */
private $cod_asignacion;


/**
 * código de la asignacion sala multiplex
 * @var [type]
 */
private $cod_asignacion_sm;


/**
* código de la Película
* @var [integer]
*/
private $cod_pelicula;


/**
* código del horario
* @var [integer]
*/
private $cod_horario;

/**
 * Constructor de la clase AignacionPHS
 * @param [Integer] $cod_asignacion [código de la asignacion]
 * @param [Integer] $cod_asignacion_sm [código de la asignacion sala multiplex]
 * @param [Integer] $cod_pelicula [código de la pelicula]
 * @param [Integer] $cod_horario [código del horario]

 */
public function __construct($cod_asignacion,$cod_asignacion_sm,$cod_pelicula, $cod_horario){
  $this->cod_asignacion=$cod_asignacion;
  $this->cod_asignacion_sm=$cod_asignacion_sm;
  $this->cod_pelicula=$cod_pelicula;
	$this->cod_horario=$cod_horario;
}

/**
 * Obtener el código de la asignacion
 * @return [Integer] [código de la asignacion]
 */
public function getCod_asignacion(){
	return $this->cod_asignacion;
}

/**
 * Modificar el código de la asignacion
 * @param [Integer] $cod_asignacion [código por el que se va a modificar]
 */
public function setCod_asignacion($cod_asignacion){
	$this->cod_asignacion=$cod_asignacion;
	return $this;
}

/**
 * Obtener el código de la asignacion
 * @return [Integer]
 */
public function getCod_asignacion_sm(){
	return $this->cod_asignacion_sm;
}

/**
 * Modificar el código de la asignacion
 * @param [Integer] $cod_asignacion_sm
 */
public function setCod_asignacion_sm($cod_asignacion_sm){
	$this->cod_asignacion_sm=$cod_asignacion_sm;
	return $this;
}
/**
 * Obtener el código de la Película
 * @return [Integer] [código de la Película]
 */
public function getCod_Pelicula(){
	return $this->cod_pelicula;
}

/**
 * Modificar el código de la Película
 * @param [Integer] $cod_pelicula [código por el que se va a modificar]
 */
public function setCod_Pelicula($cod_pelicula){
	$this->cod_pelicula=$cod_pelicula;
	return $this;
}

/**
 * Obtener el código del asiento
 * @return [Integer] [código del asiento]
 */
public function getCod_Horario(){
	return $this->cod_horario;
}

/**
 * Modificar el código del horario
 * @param [Integer] $cod_horario [código por el que se va a modificar]
 */
public function setCod_Horario($cod_horario){
	$this->cod_horario=$cod_horario;
	return $this;
}

}
?>
