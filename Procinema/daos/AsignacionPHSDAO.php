<?php

		/**
 		* Se incluye el archivo AsignacionPSA.php
  	*/
		require_once('../entidades/AsignacionPHS.php');

    /**
    * Se incluye el archivo AsignacionSM.php
    */
		require_once('../entidades/AsignacionSM.php');

    /**
    * Se incluye el archivo Pelicula.php
    */
		require_once('../entidades/Pelicula.php');

    /**
    * Se incluye el archivo Horario.php
    */
		require_once('../entidades/Horario.php');

			require_once('../util/conexion.php');


    /** Se incluye el archivo PeliculaDAO.php */
    require_once('PeliculaDAO.php');
     /** Se incluye el archivo AsinacionSMDAO.php */
    require_once('AsignacionSMDAO.php');
    /** Se incluye el archivo HorarioDAO.php */
    require_once('HorarioDAO.php');
    /**
     * Representa el DAO de la clase AsignacionPHS
     */

    class AsignacionPHSDAO {

        //------------------------------------------------------------
				//Atributos
				//-------------------------------------------------------------

        /**
         * Representa la conexion a la base de datos
         *
         * @var Object
         */

		private $con;

		//---------------------------------------------------------------
		//Constructor
		//---------------------------------------------------------------

		/**
		 * Constructor de un nuevo DAO de AsignacionPHS
		 */

		 private $conect;

	  public function __construct()
	  {
	    $this->conect = new Conexion();
	    $this->con = $this->conect->conectarBD();
	    mysqli_set_charset($this->con, "utf8");
	  }

        //----------------------------------------------------------------
        //Métodos
        //----------------------------------------------------------------
        /**
         * Crea una nueva asignacion pelicula-sala-asiento dentro de la base de datos
         *
         * @param AsignacionPHS $nuevaAsignacion
         */
        public function crearAsignacionPHS($nuevaAsignacion) {
            $sql = "INSERT INTO ASIG_PEL_HOR_SAL VALUES (0 ,".$nuevaAsignacion->getCod_asignacion_sm().",".$nuevaAsignacion->getCod_Pelicula().",".$nuevaAsignacion->getCod_Horario().")";
            mysqli_query($this->con, $sql);
        }


           /**
         * Obtiene un producto de la base de datos a partir de su código
         *
         * @param int $cod_asignacion
         * @return AsignacionPHS asignacion
       */
        public function consultarAsignacionPHS($cod_asignacion) {
            $sql = "SELECT * FROM ASIG_PEL_HOR_SAL WHERE cod_asignacion = $cod_asignacion";

            if(!$result = mysqli_query($this->con, $sql)) die();
            $row = mysqli_fetch_array($result);


            $asignacion = new AsignacionPHS($row[0], $row[1], $row[2], $row[3]);
            return $asignacion;
        }
        public function obtenerLista() {
            $sql = "SELECT * FROM ASIG_PEL_HOR_SAL";
            if(!$result = mysqli_query($this->con, $sql)) die();

			$asignaciones = array();
			while ($row = mysqli_fetch_array($result)) {

				$asignaciones[] = new AsignacionPHS($row[0], $row[1], $row[2], $row[3]);
			}
			return $asignaciones;
        }

         /**
         * Modifica una asignación de película, sala y horario
         * @param  AsignacionPHS  $asignacionphsMod
         */
           public function modificarAsignacionPHS($asignacionphsMod) {

        $sql = "UPDATE ASIG_PEL_HOR_SAL SET cod_asignacion_sm = ".$asignacionphsMod->getCod_asignacion_sm().", cod_pelicula = ".$asignacionphsMod->getCod_Pelicula().", cod_horario = ".$asignacionphsMod->getCod_Horario()." WHERE cod_asignacion = ".$asignacionphsMod->getCod_asignacion();

                    mysqli_query($this->con, $sql);



        }

        public function listaAsignacionPelicula($cod_asignacion_sm, $cod_pelicula) {
          $sql = "SELECT * FROM ASIG_PEL_HOR_SAL WHERE cod_asignacion_sm=$cod_asignacion_sm AND cod_pelicula=$cod_pelicula ORDER BY cod_horario";
          if(!$result = mysqli_query($this->con, $sql)) die();

          $asignaciones = array();
          while ($row = mysqli_fetch_array($result)) {
            $asignaciones[] = new AsignacionPHS($row[0], $row[1], $row[2], $row[3]);
          }
          return $asignaciones;
        }
  }

?>
