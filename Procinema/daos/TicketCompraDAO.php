<?php

	require_once('../entidades/TicketCompra.php');

    /** Se incluye el archivo MutiplexDAO.php */
    require_once('MultiplexDAO.php');
     /** Se incluye el archivo SalaDAO.php */
    require_once('ClienteDAO.php');
   /** Se incluye el archivo F_pagoDAO.php */
    require_once('F_PagoDAO.php');
   /** Se incluye el archivo ConfiteriaDAO.php */
    require_once('ConfiteriaDAO.php');
     /** Se incluye el archivo UsarioDAO.php */
    require_once('UsuarioDAO.php');

    /**
     * Representa el DAO de la clase TicketCompra
     */
    class TicketCompraDAO {
    //------------------------------------------------------------
		//Atributos
		//-------------------------------------------------------------

        /**
         * Representa la conexion a la base de datos
         *
         * @var Object
         */
		private $con;

		//---------------------------------------------------------------
		//Constructor
		//---------------------------------------------------------------
		/**
		 * Constructor de un nuevo DAO de ticket compra
		 */
		public function __construct($con) {
			$this->con = $con;
			mysqli_set_charset($this->con, "utf8");
		}

      //----------------------------------------------------------------
      //Métodos
      //----------------------------------------------------------------
      /**
       * Crea un nuevo ticket compra dentro de la base de datos
       *
       * @param TicketCompra $nuevoTicketget
       */
        public function crearTicketCompra($nuevoTicket) {
            $sql = "INSERT INTO TICKET_COMPRA VALUES (0,'".$nuevoTicket->getCod_Usuario()."','".$nuevoTicket->getCod_f_pago()."',
            '".$nuevoTicket->getCod_cliente()."','".$nuevoTicket->getCod_ticket_confiteria()."',
            '".$nuevoTicket->getCod_ticket_pelicula()."','".$nuevoTicket->getFecha()."','".$nuevoTicket->getPrecio()."')";
            mysqli_query($this->con, $sql);

         
        }
        /**
         * Obtiene un ticketCompra de la base de datos a partir de su código
         *
         * @param int $cod_ticket
         * @return TicketCompra ticketcompra
       */
        public function consultarTicketCompra($cod_ticket) {
            $sql = "SELECT * FROM TICKET_COMPRA WHERE cod_ticket = $cod_ticket";
            if(!$result = mysqli_query($this->con, $sql)) die();
			$row = mysqli_fetch_array($result);
          
            $ticket = new TicketCompra($row[0], $row[1],$row[2],$row[3],$row[4],$row[5],$row[6],$row[7]);
			return $ticket;
        }
        /**
         * Obtiene los tickets de la base de datos
         *
         * @return TicketCompra[] tickets
         */
        public function obtenerLista() {
            $sql = "SELECT * FROM TICKET_COMPRA";
            if(!$result = mysqli_query($this->con, $sql)) die();
			$tickets= array();
			while ($row = mysqli_fetch_array($result)) {
           
                $tickets[] = new TicketCompra($row[0], $row[1],$row[2],$row[3],$row[4],$row[5],$row[6],$row[7]);
			}
			return $tickets;
        }

        /**
         * Modifica un ticket en la base de datos
         * @param  TicketCompra $ticketMod
         */
        public function modificarTicket($ticketMod) {
            $sql = "UPDATE TICKET_COMPRA SET cod_usuario= ".$ticketMod->getCod_Usuario().",cod_f_pago= ".$ticketMod->getCod_f_pago().",cod_cliente= ".$ticketMod->getCod_cliente().",cod_ticket_confiteria= ".$ticketMod->getCod_ticket_confiteria().",cod_ticket_pelicula= ".$ticketMod->getCod_ticket_pelicula().",fecha= ".$ticketMod->getFecha().",precio=".$ticketMod->getPrecio()." WHERE cod_ticket = ".$ticketMod->getCod_ticket();
                if((!$result = mysqli_query($this->con, $sql))) die();
                $row = mysqli_fetch_array($result);
                

            mysqli_query($this->con, $sql);
        }
  }

?>
