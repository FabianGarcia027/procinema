<?php


	/**
     * Se incluye el archivo AsignacionSM.php
    */
		require_once('../entidades/AsignacionSM.php');

    /**
     * Se incluye el archivo Multiplex.php
    */
		require_once('../entidades/Multiplex.php');

    /**
     * Se incluye el archivo Sala.php
    */
		require_once('../entidades/Sala.php');



    /** Se incluye el archivo MutiplexDAO.php */
    require_once('MultiplexDAO.php');
     /** Se incluye el archivo SalaDAO.php */
    require_once('SalaDAO.php');

  /**
     * Representa el DAO de la clase AsignacionSM
     */
    class AsignacionSMDAO {
        //------------------------------------------------------------
		//Atributos
		//-------------------------------------------------------------

        /**
         * Representa la conexion a la base de datos
         *
         * @var Object
         */
		private $con;
		//---------------------------------------------------------------
		//Constructor
		//---------------------------------------------------------------

		/**
		 * Constructor de un nuevo DAO de idioma
		 */
		public function __construct($con) {

		$this->con = $con;
			mysqli_set_charset($this->con, "utf8");
		}

        //----------------------------------------------------------------
        //Métodos
        //----------------------------------------------------------------

        /**
         * Crea una nueva asignacion sala pelicula dentro de la base de datos
         *
         * @param AsignacionSM $nuevaAsignacion
         */
        public function crearAsignacionSM($nuevaAsignacion) {
            $sql = "INSERT INTO ASIG_SAL_MUL VALUES (0,'".$nuevaAsignacion->getCod_multiplex()->getCod_multiplex()."','".$nuevaAsignacion->getCod_sala()->getCod_sala()."')";
            mysqli_query($this->con, $sql);
        }


          /**
         * Obtiene un producto de la base de datos a partir de su código
         *
         * @param int $cod_asignacion
         * @return AsignacionSM asignacion
       */
        public function consultarAsignacionSM($cod_asignacion) {
            $sql = "SELECT * FROM ASIG_SAL_MUL WHERE cod_asignacion_sm = $cod_asignacion";

            if(!$result = mysqli_query($this->con, $sql)) die();
            $row = mysqli_fetch_array($result);

            $asignacion = new AsignacionSM($row[0], $row[1] ,$row[2]);
            return $asignacion;
        }


        public function obtenerLista() {
            $sql = "SELECT * FROM ASIG_SAL_MUL";
          if(!$result = mysqli_query($this->con, $sql)) die();
					$asignaciones = array();
			while ($row = mysqli_fetch_array($result)) {
                
								$asignaciones[] = new AsignacionSMDAO($row[0],$row[1], $row[2]);
			}
			return $asignaciones;
        }
    }
?>
