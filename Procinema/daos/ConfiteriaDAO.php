<?php


	/**
     * Se incluye el archivo Confiteria.php
    */
		require_once('../entidades/Confiteria.php');
		require_once('../util/Conexion.php');

    /**
     * Representa el DAO de la clase Confiteria
     */
    class ConfiteriaDAO {

		//------------------------------------------------------------
		//Atributos
		//-------------------------------------------------------------
        /**
         * Representa la conexion a la base de datos
         *
         * @var Object
         */
		private $con;

		private $conect;
		//---------------------------------------------------------------
		//Constructor
		//---------------------------------------------------------------

		/**
		 * Constructor de un nuevo DAO de poducto
		 */
		public function __construct() {
			$this->conect = new Conexion();
			$this->con = $this->conect->conectarBD();
			mysqli_set_charset($this->con, "utf8");

		}

        //----------------------------------------------------------------
        //Métodos
        //----------------------------------------------------------------

        /**
         * Crea un nuevo producto dentro de la base de datos
         *
         * @param Producto $nuevoProducto
         */
        public function crearConfiteria($nuevoProducto) {
            $sql = "INSERT INTO CONFITERIA VALUES (0,'".$nuevoProducto->getNom_confiteria()."','".$nuevoProducto->getDescripcion()."','".$nuevoProducto->getPrecio()."')";
            mysqli_query($this->con, $sql);
      }

        /**
         * Obtiene un producto de la base de datos a partir de su código
         *
         * @param int $cod_confiteria
         * @return Confiteria confiteria
       */
        public function consultarConfiteria($cod_confiteria) {
            $sql = "SELECT * FROM CONFITERIA WHERE cod_confiteria = $cod_confiteria";

            if(!$result = mysqli_query($this->con, $sql)) die();
			$row = mysqli_fetch_array($result);

            $confiteria = new Confiteria($row[0], $row[1],$row[2],$row[3]);
			return $confiteria;
        }

        /**
         * Obtiene las confiteria de la base de datos
         *
       * @return Confiteria[] confiterias
         */
        public function obtenerLista() {
            $sql = "SELECT * FROM CONFITERIA";

            if(!$result = mysqli_query($this->con, $sql)) die();
			$confiterias = array();
			while ($row = mysqli_fetch_array($result)) {
				$confiterias[] = new Confiteria($row['cod_confiteria'], $row['nom_confiteria'],$row['descripcion'],$row['precio']);
			}
			return $confiterias;
        }

      /**
         * Obtiene una confiteria de la base de datos a partir de su código
         *
         * @param String $nom_confiteria
         * @return Confiteria confiteria
         */
        public function obtenerConfiteriaDAO($nom_confiteria) {
            $sql = "SELECT * FROM CONFITERIA WHERE nom_confiteria = $nom_confiteria";

            if(!$result = mysqli_query($this->con, $sql)) {
                return null;
            }

		$row = mysqli_fetch_array($result);

            $confiteria = new Confiteria($row[0], $row[1],$row[2],$row[3]);
			return $confiteria;
        }

       /**
         * Modifica a un producto en la base de datos
       * @param  Producto  $productoMod
         */
        public function modificarConfiteria($productoMod) {
        
                $sql = "UPDATE CONFITERIA SET nom_confiteria = '".$productoMod->getNom_confiteria()."', descripcion = '".$productoMod->getDescripcion().
                  "', precio = ".$productoMod->getPrecio().
                    " WHERE cod_confiteria= ".$productoMod->getCod_confiteria();

          mysqli_query($this->con, $sql);
        }

    }

?>
