<?php

/**
 * Se incluye el archivo Asiento.php
 */
require_once('../entidades/Asiento.php');

/**
 * Se incluye el archivo T_Silla.php
 */
require_once('../entidades/T_Silla.php');

/** Se incluye el archivo T_SillaDAO.php */

require_once('T_SillaDAO.php');
/**
 * Representa el DAO de la clase Asiento
 */

class AsientoDAO
{
    //------------------------------------------------------------
    //Atributos
    //-------------------------------------------------------------

    /**
     * Representa la conexion a la base de datos
     *
     * @var Object
     */
    private $con;

    //---------------------------------------------------------------
    //Constructor
    //---------------------------------------------------------------

    /**
     * Constructor de un nuevo DAO de asiento
     */
    public function __construct($con)
    {
        $this->con = $con;
        mysqli_set_charset($this->con, "utf8");
    }
    //----------------------------------------------------------------
    //Métodos
    //----------------------------------------------------------------

    /**
     * Crea un nuevo asiento dentro de la base de datos
     *
     * @param Asiento $nuevoAsiento
     */
    public function crearAsiento($nuevoAsiento)
    {
        $sql = "INSERT INTO ASIENTO VALUES (0,'" . $nuevoAsiento->getCod_Asiento() . "','" . $nuevoAsiento->getNom_Asiento() ."','" . $nuevoAsiento->getDisponilidad() . "')";
        mysqli_query($this->con, $sql);
    }

    /**
     * Obtiene un asiento  de la base de datos a partir de su código
     *
     * @param int $cod_asiento
     * @return Asiento asiento
     */
    public function consultarAsiento($cod_asiento)
    {
        $sql = "SELECT * FROM ASIENTO WHERE cod_asiento = $cod_asiento";
        if (!$result = mysqli_query($this->con, $sql)) die();
        $row = mysqli_fetch_array($result);
        $tipoSillaDAO = new T_SillaDAO($this->con);
        $tipoSilla = $tipoSillaDAO->consultarTSilla($row[1]);
        $asiento = new Asiento($row[0], $tipoSilla, $row[2], $row[3]);
        return $asiento;
    }

    /**
     * Obtiene el asiento de la base de datos
     *
     * @return Asiento[] asientos
     */
    public function obtenerLista()
    {
        $sql = "SELECT * FROM ASIENTO";
        if (!$result = mysqli_query($this->con, $sql)) die();
        $asientos = array();
        while ($row = mysqli_fetch_array($result)) {
            $tipoSillaDAO = new T_SillaDAO($this->con);
            $tipoSilla = $tipoSillaDAO->consultarTSilla($row[1]);
            $asientos[] = new Asiento($row[0], $tipoSilla, $row[2], $row[3]);
        }
        return $asientos;
    }

    public function listaA5()
    {
        $sql = "SELECT * FROM ASIENTO LIMIT 5";
        if (!$result = mysqli_query($this->con, $sql)) die();
        $asientos = array();
        while ($row = mysqli_fetch_array($result)) {
            $tipoSillaDAO = new T_SillaDAO($this->con);
            $tipoSilla = $tipoSillaDAO->consultarTSilla($row[1]);
            $asientos[] = new Asiento($row[0], $row[1], $row[2], $row[3]);
        }
        return $asientos;
    }
    public function listaA7()
    {
        $sql = "SELECT * from ASIENTO LIMIT 7 OFFSET 5";
        if (!$result = mysqli_query($this->con, $sql)) die();
        $asientos = array();
        while ($row = mysqli_fetch_array($result)) {
            $tipoSillaDAO = new T_SillaDAO($this->con);
            $tipoSilla = $tipoSillaDAO->consultarTSilla($row[1]);
            $asientos[] = new Asiento($row[0], $row[1], $row[2], $row[3]);
        }
        return $asientos;
    }
    public function listaB5()
    {
        $sql = "SELECT * from ASIENTO LIMIT 5 OFFSET 12";
        if (!$result = mysqli_query($this->con, $sql)) die();
        $asientos = array();
        while ($row = mysqli_fetch_array($result)) {
            $tipoSillaDAO = new T_SillaDAO($this->con);
            $tipoSilla = $tipoSillaDAO->consultarTSilla($row[1]);
            $asientos[] = new Asiento($row[0], $row[1], $row[2], $row[3]);
        }
        return $asientos;
    }
    public function listaB7()
    {
        $sql = "SELECT * from ASIENTO LIMIT 7 OFFSET 17";
        if (!$result = mysqli_query($this->con, $sql)) die();
        $asientos = array();
        while ($row = mysqli_fetch_array($result)) {
            $tipoSillaDAO = new T_SillaDAO($this->con);
            $tipoSilla = $tipoSillaDAO->consultarTSilla($row[1]);
            $asientos[] = new Asiento($row[0], $row[1], $row[2], $row[3]);
        }
        return $asientos;
    }
    public function listaC5()
    {
        $sql = "SELECT * from ASIENTO LIMIT 5 OFFSET 24";
        if (!$result = mysqli_query($this->con, $sql)) die();
        $asientos = array();
        while ($row = mysqli_fetch_array($result)) {
            $tipoSillaDAO = new T_SillaDAO($this->con);
            $tipoSilla = $tipoSillaDAO->consultarTSilla($row[1]);
            $asientos[] = new Asiento($row[0], $row[1], $row[2], $row[3]);
        }
        return $asientos;
    }
    public function listaC7()
    {
        $sql = "SELECT * from ASIENTO LIMIT 7 OFFSET 29";
        if (!$result = mysqli_query($this->con, $sql)) die();
        $asientos = array();
        while ($row = mysqli_fetch_array($result)) {
            $tipoSillaDAO = new T_SillaDAO($this->con);
            $tipoSilla = $tipoSillaDAO->consultarTSilla($row[1]);
            $asientos[] = new Asiento($row[0], $row[1], $row[2], $row[3]);
        }
        return $asientos;
    }
    public function listaD5()
    {
        $sql = "SELECT * from ASIENTO LIMIT 5 OFFSET 36";
        if (!$result = mysqli_query($this->con, $sql)) die();
        $asientos = array();
        while ($row = mysqli_fetch_array($result)) {
            $tipoSillaDAO = new T_SillaDAO($this->con);
            $tipoSilla = $tipoSillaDAO->consultarTSilla($row[1]);
            $asientos[] = new Asiento($row[0], $row[1], $row[2], $row[3]);
        }
        return $asientos;
    }
    public function listaD7()
    {
        $sql = "SELECT * from ASIENTO LIMIT 7 OFFSET 41";
        if (!$result = mysqli_query($this->con, $sql)) die();
        $asientos = array();
        while ($row = mysqli_fetch_array($result)) {
            $tipoSillaDAO = new T_SillaDAO($this->con);
            $tipoSilla = $tipoSillaDAO->consultarTSilla($row[1]);
            $asientos[] = new Asiento($row[0], $row[1], $row[2], $row[3]);
        }
        return $asientos;
    }
    public function listaE5()
    {
        $sql = "SELECT * from ASIENTO LIMIT 5 OFFSET 48";
        if (!$result = mysqli_query($this->con, $sql)) die();
        $asientos = array();
        while ($row = mysqli_fetch_array($result)) {
            $tipoSillaDAO = new T_SillaDAO($this->con);
            $tipoSilla = $tipoSillaDAO->consultarTSilla($row[1]);
            $asientos[] = new Asiento($row[0], $row[1], $row[2], $row[3]);
        }
        return $asientos;
    }
    public function listaE7()
    {
        $sql = "SELECT * from ASIENTO LIMIT 7 OFFSET 53";
        if (!$result = mysqli_query($this->con, $sql)) die();
        $asientos = array();
        while ($row = mysqli_fetch_array($result)) {
            $tipoSillaDAO = new T_SillaDAO($this->con);
            $tipoSilla = $tipoSillaDAO->consultarTSilla($row[1]);
            $asientos[] = new Asiento($row[0], $row[1], $row[2], $row[3]);
        }
        return $asientos;
    }
    public function listaF5()
    {
        $sql = "SELECT * from ASIENTO LIMIT 5 OFFSET 60";
        if (!$result = mysqli_query($this->con, $sql)) die();
        $asientos = array();
        while ($row = mysqli_fetch_array($result)) {
            $tipoSillaDAO = new T_SillaDAO($this->con);
            $tipoSilla = $tipoSillaDAO->consultarTSilla($row[1]);
            $asientos[] = new Asiento($row[0], $row[1], $row[2], $row[3]);
        }
        return $asientos;
    }
    public function listaF7()
    {
        $sql = "SELECT * from ASIENTO LIMIT 7 OFFSET 65";
        if (!$result = mysqli_query($this->con, $sql)) die();
        $asientos = array();
        while ($row = mysqli_fetch_array($result)) {
            $tipoSillaDAO = new T_SillaDAO($this->con);
            $tipoSilla = $tipoSillaDAO->consultarTSilla($row[1]);
            $asientos[] = new Asiento($row[0], $row[1], $row[2], $row[3]);
        }
        return $asientos;
    }
    public function listaG5()
    {
        $sql = "SELECT * from ASIENTO LIMIT 5 OFFSET 72";
        if (!$result = mysqli_query($this->con, $sql)) die();
        $asientos = array();
        while ($row = mysqli_fetch_array($result)) {
            $tipoSillaDAO = new T_SillaDAO($this->con);
            $tipoSilla = $tipoSillaDAO->consultarTSilla($row[1]);
            $asientos[] = new Asiento($row[0], $row[1], $row[2], $row[3]);
        }
        return $asientos;
    }
    public function listaG7()
    {
        $sql = "SELECT * from ASIENTO LIMIT 7 OFFSET 77";
        if (!$result = mysqli_query($this->con, $sql)) die();
        $asientos = array();
        while ($row = mysqli_fetch_array($result)) {
            $tipoSillaDAO = new T_SillaDAO($this->con);
            $tipoSilla = $tipoSillaDAO->consultarTSilla($row[1]);
            $asientos[] = new Asiento($row[0], $row[1], $row[2], $row[3]);
        }
        return $asientos;
    }
    public function listaH5()
    {
        $sql = "SELECT * from ASIENTO LIMIT 5 OFFSET 84";
        if (!$result = mysqli_query($this->con, $sql)) die();
        $asientos = array();
        while ($row = mysqli_fetch_array($result)) {
            $tipoSillaDAO = new T_SillaDAO($this->con);
            $tipoSilla = $tipoSillaDAO->consultarTSilla($row[1]);
            $asientos[] = new Asiento($row[0], $row[1], $row[2], $row[3]);
        }
        return $asientos;
    }
    public function listaH7()
    {
        $sql = "SELECT * from ASIENTO LIMIT 7 OFFSET 89";
        if (!$result = mysqli_query($this->con, $sql)) die();
        $asientos = array();
        while ($row = mysqli_fetch_array($result)) {
            $tipoSillaDAO = new T_SillaDAO($this->con);
            $tipoSilla = $tipoSillaDAO->consultarTSilla($row[1]);
            $asientos[] = new Asiento($row[0], $row[1], $row[2], $row[3]);
        }
        return $asientos;
    }
    public function listaI5()
    {
        $sql = "SELECT * from ASIENTO LIMIT 5 OFFSET 96";
        if (!$result = mysqli_query($this->con, $sql)) die();
        $asientos = array();
        while ($row = mysqli_fetch_array($result)) {
            $tipoSillaDAO = new T_SillaDAO($this->con);
            $tipoSilla = $tipoSillaDAO->consultarTSilla($row[1]);
            $asientos[] = new Asiento($row[0], $row[1], $row[2], $row[3]);
        }
        return $asientos;
    }
    public function listaI7()
    {
        $sql = "SELECT * from ASIENTO LIMIT 7 OFFSET 101";
        if (!$result = mysqli_query($this->con, $sql)) die();
        $asientos = array();
        while ($row = mysqli_fetch_array($result)) {
            $tipoSillaDAO = new T_SillaDAO($this->con);
            $tipoSilla = $tipoSillaDAO->consultarTSilla($row[1]);
            $asientos[] = new Asiento($row[0], $row[1], $row[2], $row[3]);
        }
        return $asientos;
    }
    public function listaJ5()
    {
        $sql = "SELECT * from ASIENTO LIMIT 5 OFFSET 108";
        if (!$result = mysqli_query($this->con, $sql)) die();
        $asientos = array();
        while ($row = mysqli_fetch_array($result)) {
            $tipoSillaDAO = new T_SillaDAO($this->con);
            $tipoSilla = $tipoSillaDAO->consultarTSilla($row[1]);
            $asientos[] = new Asiento($row[0], $row[1], $row[2], $row[3]);
        }
        return $asientos;
    }
    public function listaJ7()
    {
        $sql = "SELECT * from ASIENTO LIMIT 7 OFFSET 113";
        if (!$result = mysqli_query($this->con, $sql)) die();
        $asientos = array();
        while ($row = mysqli_fetch_array($result)) {
            $tipoSillaDAO = new T_SillaDAO($this->con);
            $tipoSilla = $tipoSillaDAO->consultarTSilla($row[1]);
            $asientos[] = new Asiento($row[0], $row[1], $row[2], $row[3]);
        }
        return $asientos;
    }
    /**
     * Modifica un asiento de pago en la base de datos
     * @param  Asiento $asientoMod
     */
    public function modificarAsiento($asientoMod)
    {
        $sql = "UPDATE ASIENTO SET cod_t_silla = " . $asientoMod->getCod_Asiento() . ",disponibilidad=" . $asientoMod->getDisponilidad() . "
                 WHERE cod_asiento = " . $asientoMod->getCod_Asiento();
        if ((!$result = mysqli_query($this->con, $sql))) die();
        $row = mysqli_fetch_array($result);
        mysqli_query($this->con, $sql);
    }
}
