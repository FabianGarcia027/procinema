<?php

    require_once('../entidades/TicketPelicula.php');

    /** Se incluye el archivo AsignacionPHSDAO.php */
    require_once('AsignacionPHSDAO.php');
     /** Se incluye el archivo AsientoDAO.php */
    require_once('AsientoDAO.php');
     
    

    /**
     * Representa el DAO de la clase TicketPelicula
     */
    class TicketPeliculaDAO {
    //------------------------------------------------------------
		//Atributos
		//-------------------------------------------------------------

        /**
         * Representa la conexion a la base de datos
         *
         * @var Object
         */
		private $con;

		//---------------------------------------------------------------
		//Constructor
		//---------------------------------------------------------------
		/**
		 * Constructor de un nuevo DAO de ticket pelicula
		 */
		public function __construct($con) {
			$this->con = $con;
			mysqli_set_charset($this->con, "utf8");
		}

      //----------------------------------------------------------------
      //Métodos
      //----------------------------------------------------------------
      /**
       * Crea un nuevo ticket compra dentro de la base de datos
       *
       * @param TicketPelicula $nuevoTicket
       */
        public function crearTicketCompra($nuevoTicket) {            
            $sql = "INSERT INTO TICKET_PELICULA VALUES (0,".$nuevoTicket->getCod_asignacion().",".$nuevoTicket->getCod_asiento().",".$nuevoTicket->getPrecio().")";            
            mysqli_query($this->con, $sql);
        }
        /**
         * Obtiene un ticketPelicula de la base de datos a partir de su código
         *
         * @param int $cod_ticket
         * @return TicketPelicula ticketcompra
       */
        public function consultarTicketPelicula($cod_ticket) {
            $sql = "SELECT * FROM TICKET_PELICULA WHERE cod_ticket_pelicula = $cod_ticket";
            if(!$result = mysqli_query($this->con, $sql)) die();
						$row = mysqli_fetch_array($result);
          	$asignacionDAO= new AsignacionPHSDAO($this->con);
            $asignacion=$asignacionDAO->consultarAsignacionPHS($row[1]);
            $asientoDAO= new AsientoDAO($this->con);
            $asiento=$asientoDAO->consultarAsiento($row[2]);
            

            $ticket = new TicketPelicula($row[0], $asignacion,$asiento,$row[3]);
			return $ticket;
        }



        
        /**
         * Obtiene los tickets de la base de datos
         *
         * @return TicketPelicula[] tickets
         */
        public function obtenerLista() {
            $sql = "SELECT * FROM TICKET_PELICULA";
            if(!$result = mysqli_query($this->con, $sql)) die();
			$tickets= array();
			while ($row = mysqli_fetch_array($result)) {

            $asignacionDAO= new AsignacionPHSDAO($this->con);
            $asignacion=$asignacionDAO->consultarAsignacionPHS($row[1]);
            $asientoDAO= new AsientoDAO($this->con);
            $asiento=$asientoDAO->consultarAsiento($row[2]);
            

            $tickets[] = new TicketPelicula($row[0], $asignacion,$asiento,$row[3]);
                
			}
			return $tickets;
        }
        public function obtenerUltimo() {
            $sql = "SELECT * FROM TICKET_PELICULA order by cod_ticket_pelicula desc limit 1";
            if(!$result = mysqli_query($this->con, $sql)) die();
			$tickets= array();
			while ($row = mysqli_fetch_array($result)) {

            $asignacionDAO= new AsignacionPHSDAO($this->con);
            $asignacion=$asignacionDAO->consultarAsignacionPHS($row[1]);
            $asientoDAO= new AsientoDAO($this->con);
            $asiento=$asientoDAO->consultarAsiento($row[2]);
            

            $tickets[] = new TicketPelicula($row[0], $asignacion,$asiento,$row[3]);
                
			}
			return $tickets;
        }
        /**
         * Modifica un ticket en la base de datos
         * @param  TicketPelicula $ticketMod
         */
        public function modificarTicket($ticketMod) {
            $sql = "UPDATE TICKET_PELICULA SET cod_asignacion= ".$ticketMod->getCod_asignacion().",cod_asiento= ".$ticketMod->getCod_asiento().",precio=".$ticketMod->getPrecio()." WHERE cod_ticket_pelicula = ".$ticketMod->getCod_ticket_pelicula();
            if((!$result = mysqli_query($this->con, $sql))) die();
            $row = mysqli_fetch_array($result);                
            mysqli_query($this->con, $sql);
        }
  }

?>
