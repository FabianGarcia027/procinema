<?php

require_once('../entidades/TicketConfiteria.php');


/** Se incluye el archivo ConfiteriaDAO.php */
require_once('ConfiteriaDAO.php');


/**
 * Representa el DAO de la clase TicketConfiteria
 */
class TicketConfiteriaDAO
{
  //------------------------------------------------------------
  //Atributos
  //-------------------------------------------------------------

  /**
   * Representa la conexion a la base de datos
   *
   * @var Object
   */
  private $con;

  //---------------------------------------------------------------
  //Constructor
  //---------------------------------------------------------------
  /**
   * Constructor de un nuevo DAO de ticket confiteria
   */
  public function __construct($con)
  {
    $this->con = $con;
    mysqli_set_charset($this->con, "utf8");
  }

  //----------------------------------------------------------------
  //Métodos
  //----------------------------------------------------------------
  /**
   * Crea un nuevo ticket confiteria dentro de la base de datos
   *
   * @param TicketConfiteria $nuevoTicket
   */
  public function crearTicketConfiteria($nuevoTicket)
  {
    $sql = "INSERT INTO TICKET_CONFITERIA VALUES (0,'" . $nuevoTicket->getCod_confiteria() . "','" . $nuevoTicket->getPrecio() . "')";
    mysqli_query($this->con, $sql);
  }
  /**
   * Obtiene un ticketConfiteria de la base de datos a partir de su código
   *
   * @param int $cod_ticket
   * @return TicketConfiteria ticketconfiteria
   */
  public function consultarTicketConfiteria($cod_ticket)
  {
    $sql = "SELECT * FROM TICKET_CONFITERIA WHERE cod_ticket_confiteria = $cod_ticket";
    if (!$result = mysqli_query($this->con, $sql)) die();
    $row = mysqli_fetch_array($result);

    $confiteriaDAO = new ConfiteriaDAO($this->con);
    $confiteria = $confiteriaDAO->consultarConfiteria($row[1]);


    $ticket = new TicketConfiteria($row[0], $confiteria, $row[2]);
    return $ticket;
  }
  /**
   * Obtiene los tickets de la base de datos
   *
   * @return TicketConfiteria[] tickets
   */
  public function obtenerLista()
  {
    $sql = "SELECT * FROM TICKET_CONFITERIA";
    if (!$result = mysqli_query($this->con, $sql)) die();
    $tickets = array();
    while ($row = mysqli_fetch_array($result)) {

      $confiteriaDAO = new ConfiteriaDAO($this->con);
      $confiteria = $confiteriaDAO->consultarConfiteria($row[1]);

      $ticket[] = new TicketConfiteria($row[0], $confiteria, $row[2]);
    }
    return $tickets;
  }

  public function obtenerUltimo()
  {
    $sql = "SELECT * FROM TICKET_CONFITERIA order by cod_ticket_confiteria desc limit 1";
    if (!$result = mysqli_query($this->con, $sql)) die();
    $tickets = array();
    while ($row = mysqli_fetch_array($result)) {
      
      $confiteriaDAO = new ConfiteriaDAO($this->con);
      $confiteria = $confiteriaDAO->consultarConfiteria($row[1]);

      $ticket[] = new TicketConfiteria($row[0], $confiteria, $row[2]);


      $tickets[] = new TicketConfiteria($row[0], $confiteria, $row[2]);
    }
    return $tickets;
  }
  /**
   * Modifica un ticket en la base de datos
   * @param  TicketConfiteria $ticketMod
   */
  public function modificarTicket($ticketMod)
  {
    $sql = "UPDATE TICKET_CONFITERIA SET cod_confiteria= " . $ticketMod->getCod_confiteria()->getCod_confiteria() . ",precio=" . $ticketMod->getPrecio() . " WHERE cod_ticket_confiteria = " . $ticketMod->getCod_ticket_confiteria();
    if ((!$result = mysqli_query($this->con, $sql))) die();
    $row = mysqli_fetch_array($result);

    mysqli_query($this->con, $sql);
  }
}
