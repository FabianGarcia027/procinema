<?php
	/**
	 * Se incluye el archivo Conexion.php
	 */
	require_once('../util/Conexion.php');

	/** Se incluye el archivo Auditoria.php */
	require_once('../entidades/Auditoria.php');

	/**
	 * Representa el DAO de la entidad auditoria
	 */
	class AuditoriaDAO{

		//------------------------------------------------------------
		//Atributos
		//-------------------------------------------------------------

		/**
		 * Representa la conexion a la base de datos
		 */
		private $conexion;

		/** @var [Conexion] Representa el objecto de la clase Conexion */
		private $conect;

		//---------------------------------------------------------------
		//Constructor
		//---------------------------------------------------------------

		/**
		 * Constructor de un nuevo DAO de auditoria
		 */
		public function __construct() {
			$this->conect = new Conexion();
			$this->conexion = $this->conect->conectarBD();
			mysqli_set_charset($this->conexion, "utf8");
		}

		//--------------------------------------------------------------------
		//Métodos
		//--------------------------------------------------------------------

		/**
		 * Crea una nueva fila auditoria en la base de datos
		 * @param  Auditoria $nuevaAuditoria Auditoria a insertar en la base de datos
		 */
		public function crearAuditoria($nuevaAuditoria) {
			$sql = "INSERT INTO AUDITORIA VALUES ('".$nuevaAuditoria->getCod_usuario()."','".$nuevaAuditoria->getCod_t_usuario()."', '".$nuevaAuditoria->getNom_usuario()."', '".$nuevaAuditoria->getOperacion()."', '".$nuevaAuditoria->getFecha_operacion()."', '".$nuevaAuditoria->getIp()."')";
			mysqli_query($this->conexion, $sql);
			$this->conect->desconectarBD($this->conexion);
			unset($this->conect);
		}

		/**
		 * Obtiene a las Auditorias de la base de datos
		 * @return Auditoria[] $Auditorias Devuelve la lista de Auditorias de la base de datos
		 */
		public function obtenerAuditorias() {
			$sql = "SELECT * FROM AUDITORIA";
			if(!$result = mysqli_query($this->conexion, $sql)) die();
			$auditorias = array();
			while ($row = mysqli_fetch_array($result)) {
				$auditorias[] = new Auditoria($row["cod_usuario"],$row["cod_t_usuario"], $row["nom_usuario"], $row["operacion"], $row["fecha_operacion"], $row["ip"]);
			}
			$this->conect->desconectarBD($this->conexion);
			unset($this->conect);
			return $auditorias;
		}

		/**
		 * Obtiene las Auditorias de la base de datos filtradas
		 * @param  [String] $filtro Filtro para obtener las auditorias
		 * @return [Auditoria[]]  $Auditorias Devuelve la lista de Auditorias de la base de datos
		 */
		public function obtenerAuditoriasFiltradas($filtro) {
			$sql = "SELECT * FROM AUDITORIA WHERE ".$filtro;
			if(!$result = mysqli_query($this->conexion, $sql)) die();
			$auditorias = array();
			while ($row = mysqli_fetch_array($result)) {
				$auditorias[] = new Auditoria($row["cod_usuario"],$row["cod_t_usuario"], $row["nom_usuario"], $row["operacion"], $row["fecha_operacion"], $row["ip"]);
			}
			$this->conect->desconectarBD($this->conexion);
			unset($this->conect);
			return $auditorias;
		}
	}
?>
