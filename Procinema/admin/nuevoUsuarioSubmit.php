<html>
<?php
session_start();

require_once('../daos/UsuarioDAO.php');
require_once('../entidades/Usuario.php');
require_once('../util/Conexion.php');
require_once($_SERVER['DOCUMENT_ROOT'] .'/procinema/Procinema/daos/AuditoriaDAO.php');
require_once($_SERVER['DOCUMENT_ROOT'] .'/procinema/Procinema/entidades/Auditoria.php');

$conexion = new Conexion();
$con = $conexion->conectarBD();

$codigo=$_POST["codigo"];
$nombre=$_POST["nombre"];
$salario=$_POST["salario"];
$fecha=$_POST["fecha"];
$password=$_POST["password"];
$correo=$_POST["correo"];

$fecha_auditoria = date('Y-m-d H:i:s');
$ip = $_SERVER['REMOTE_ADDR'];

$usuarioDAO = new UsuarioDAO($con);
$usuarioNuevo = new Usuario($codigo, $nombre, 1, $_SESSION['multiplex'],  $salario, $fecha, $password, $correo);
$usuarioDAO->crearUsuario($usuarioNuevo);

$auditoriaDAO = new AuditoriaDAO($con);
$nuevaAuditoria = new Auditoria($_SESSION['codigo'],0,$_SESSION['nombre_admin'], 'R',$fecha_auditoria,$ip);
$auditoriaDAO->crearAuditoria($nuevaAuditoria);


?>
<meta http-equiv="refresh" content="0; url=usuarios.php" />
</html>
