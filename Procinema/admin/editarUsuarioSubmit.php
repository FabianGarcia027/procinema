<html>
<?php
session_start();

require_once('../daos/UsuarioDAO.php');
require_once('../entidades/Usuario.php');
require_once('../util/Conexion.php');
require_once($_SERVER['DOCUMENT_ROOT'] .'/procinema/Procinema/daos/AuditoriaDAO.php');
require_once($_SERVER['DOCUMENT_ROOT'] .'/procinema/Procinema/entidades/Auditoria.php');

$conexion = new Conexion();
$con = $conexion->conectarBD();

$codigo=$_GET["codigo"];
$nombre=$_POST["nombre"];
$salario=$_POST["salario"];
$fecha=$_POST["fecha"];
$password=$_POST["password"];
$correo=$_POST["correo"];
$fecha_auditoria = date('Y-m-d H:i:s');
$ip = $_SERVER['REMOTE_ADDR'];

$usuarioDAO = new UsuarioDAO($con);
$usuarioEditado = new Usuario($codigo, $nombre, $usuarioDAO->consultarUsuario($codigo)->getCod_t_usuario(), $usuarioDAO->consultarUsuario($codigo)->getCod_Multiplex(), $salario, $fecha,  $password, $correo);
$usuarioDAO->modificarUsuario($usuarioEditado);

$auditoriaDAO = new AuditoriaDAO($con);
$nuevaAuditoria = new Auditoria($_SESSION['codigo'],0,$_SESSION['nombre_admin'], 'U',$fecha_auditoria,$ip);
$auditoriaDAO->crearAuditoria($nuevaAuditoria);

?>
<meta http-equiv="refresh" content="0; url=usuarios.php" />
</html>
