<html>
<?php
session_start();

require_once($_SERVER['DOCUMENT_ROOT'] .'/procinema/Procinema/daos/ConfiteriaDAO.php');
require_once($_SERVER['DOCUMENT_ROOT'] .'/procinema/Procinema/entidades/Confiteria.php');
require_once($_SERVER['DOCUMENT_ROOT'] .'/procinema/Procinema/daos/AuditoriaDAO.php');
require_once($_SERVER['DOCUMENT_ROOT'] .'/procinema/Procinema/entidades/Auditoria.php');
require_once($_SERVER['DOCUMENT_ROOT'] .'/procinema/Procinema/util/Conexion.php');

$conexion = new Conexion();
$con = $conexion->conectarBD();

$codigo=$_GET["codigo"];
$nombre=$_POST["nombre"];
$descripcion=$_POST["descripcion"];
$precio=$_POST["precio"];

$fecha_auditoria = date('Y-m-d H:i:s');
$ip = $_SERVER['REMOTE_ADDR'];;
$nombre = $_SESSION['nombre_admin'];

//$asignacionDAO = new ConfiteriaDAO($con);
//$confiteriaEditada = new Confiteria($codigo,$nombre,$descripcion,$precio);
//$asignacionDAO->modificarConfiteria($confiteriaEditada);

$auditoriaDAO = new AuditoriaDAO($con);
$nuevaAuditoria = new Auditoria($_SESSION['codigo'],0,$nombre,'U',$fecha_auditoria,$ip);
$auditoriaDAO->crearAuditoria($nuevaAuditoria);



?>
<meta http-equiv="refresh" content="0; url=confiteria.php" />
</html>
