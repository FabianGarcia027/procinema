<html>
<?php 
session_start();

require_once('../daos/AsignacionPHSDAO.php');
require_once('../entidades/AsignacionPHS.php');
require_once($_SERVER['DOCUMENT_ROOT'] .'/procinema/Procinema/daos/AuditoriaDAO.php');
require_once($_SERVER['DOCUMENT_ROOT'] .'/procinema/Procinema/entidades/Auditoria.php');
require_once('../util/Conexion.php');


$conexion = new Conexion();
$con = $conexion->conectarBD();


$pelicula=$_POST["pelicula"];
$horario=$_POST["horario"];
$sala=$_POST["sala"];

$fecha_auditoria = date('Y-m-d H:i:s');
$ip = $_SERVER['REMOTE_ADDR'];;

$query = $con->query("SELECT cod_asignacion_sm FROM ASIG_SAL_MUL,SALA WHERE ASIG_SAL_MUL.cod_sala = SALA.cod_sala AND SALA.cod_sala=".$sala.";");
$row = mysqli_fetch_array($query);

$asignacionDAO = new AsignacionPHSDAO($con);
$asignacionNueva = new AsignacionPHS(0, $row[0], $pelicula,$horario);
$asignacionDAO->crearAsignacionPHS($asignacionNueva);

$auditoriaDAO = new AuditoriaDAO($con);
$nuevaAuditoria = new Auditoria($_SESSION['codigo'],0,$_SESSION['nombre_admin'], 'R',$fecha_auditoria,$ip);
$auditoriaDAO->crearAuditoria($nuevaAuditoria);

?>
<meta http-equiv="refresh" content="0; url=asignaciones.php" />
</html>