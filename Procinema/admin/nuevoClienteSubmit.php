<html>
<?php
session_start();

require_once('../daos/ClienteDAO.php');
require_once('../entidades/Cliente.php');
require_once('../util/Conexion.php');
require_once($_SERVER['DOCUMENT_ROOT'] .'/procinema/Procinema/daos/AuditoriaDAO.php');
require_once($_SERVER['DOCUMENT_ROOT'] .'/procinema/Procinema/entidades/Auditoria.php');

$conexion = new Conexion();
$con = $conexion->conectarBD();

$nombre=$_POST["nombre"];
$correo=$_POST["correo"];
$puntos=$_POST["puntos"];

$fecha_auditoria = date('Y-m-d H:i:s');
$ip = $_SERVER['REMOTE_ADDR'];

$clienteDAO = new ClienteDAO($con);
$clienteNuevo = new Cliente(0, $nombre, $correo, $puntos);
$clienteDAO->crearCliente($clienteNuevo);


$auditoriaDAO = new AuditoriaDAO($con);
$nuevaAuditoria = new Auditoria($_SESSION['codigo'],0,$_SESSION['nombre_admin'], 'R',$fecha_auditoria,$ip);
$auditoriaDAO->crearAuditoria($nuevaAuditoria);


?>
<meta http-equiv="refresh" content="0; url=clientes.php" />
</html>
