<html>
<?php 
session_start();

require_once($_SERVER['DOCUMENT_ROOT'] .'/procinema/Procinema/daos/PeliculaDAO.php');
require_once($_SERVER['DOCUMENT_ROOT'] .'/procinema/Procinema/entidades/Pelicula.php');
require_once($_SERVER['DOCUMENT_ROOT'] .'/procinema/Procinema/daos/AuditoriaDAO.php');
require_once($_SERVER['DOCUMENT_ROOT'] .'/procinema/Procinema/entidades/Auditoria.php');
require_once($_SERVER['DOCUMENT_ROOT'] .'/procinema/Procinema/util/Conexion.php');

$conexion = new Conexion();
$con = $conexion->conectarBD();

$nombre=$_POST["nombre"];
$genero=$_POST["selectG"];
$idioma=$_POST["selectI"];
$descripcion=$_POST["descripcion"];
$duracion=$_POST["duracion"];
$fecha=$_POST["fecha"];
$calificacion=$_POST["calificacion"];

$fecha_auditoria = date('Y-m-d H:i:s');
$ip = $_SERVER['REMOTE_ADDR'];;



$peliculaDAO = new PeliculaDAO($con);
$peliculaNueva = new Pelicula(0, $nombre, $genero, $idioma,$descripcion,$duracion,$fecha,$calificacion);
$peliculaDAO->crearPelicula($peliculaNueva);


$auditoriaDAO = new AuditoriaDAO($con);
$nuevaAuditoria = new Auditoria($_SESSION['codigo'],0,$_SESSION['nombre_admin'], 'R',$fecha_auditoria,$ip);
$auditoriaDAO->crearAuditoria($nuevaAuditoria);


?>
<meta http-equiv="refresh" content="0; url=peliculas.php" />
</html>
