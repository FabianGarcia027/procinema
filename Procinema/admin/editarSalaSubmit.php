<html>
<?php
session_start();

require_once('../entidades/Sala.php');
require_once('../daos/SalaDAO.php');
require_once($_SERVER['DOCUMENT_ROOT'] .'/procinema/Procinema/daos/AuditoriaDAO.php');
require_once($_SERVER['DOCUMENT_ROOT'] .'/procinema/Procinema/entidades/Auditoria.php');
require_once('../util/Conexion.php');


$conexion = new Conexion();
$con = $conexion->conectarBD();

$codigo=$_GET["codigo"];
$nombre=$_POST["nombre"];

$fecha_auditoria = date('Y-m-d H:i:s');
$ip = $_SERVER['REMOTE_ADDR'];;

$salaDAO = new SalaDAO($con);
$salaEditado = new Sala($codigo,$nombre);
$salaDAO->modificarSala($salaEditado);

$auditoriaDAO = new AuditoriaDAO($con);
$nuevaAuditoria = new Auditoria($_SESSION['codigo'],0,$_SESSION['nombre_admin'], 'U',$fecha_auditoria,$ip);
$auditoriaDAO->crearAuditoria($nuevaAuditoria);

?>
<meta http-equiv="refresh" content="0; url=salas.php" />
</html>
