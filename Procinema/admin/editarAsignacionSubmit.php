<html>
<?php
session_start();

require_once($_SERVER['DOCUMENT_ROOT'] .'/procinema/Procinema/daos/AsignacionPHSDAO.php');
require_once($_SERVER['DOCUMENT_ROOT'] .'/procinema/Procinema/entidades/AsignacionPHS.php');
require_once($_SERVER['DOCUMENT_ROOT'] .'/procinema/Procinema/daos/AuditoriaDAO.php');
require_once($_SERVER['DOCUMENT_ROOT'] .'/procinema/Procinema/entidades/Auditoria.php');
require_once($_SERVER['DOCUMENT_ROOT'] .'/procinema/Procinema/util/Conexion.php');
$conexion = new Conexion();
$con = $conexion->conectarBD();

$codigo=$_GET["codigo"];
$pelicula=$_POST["pelicula"];
$sala=$_POST["sala"];
$horario=$_POST["horario"];

$fecha_auditoria = date('Y-m-d H:i:s');
$ip = $_SERVER['REMOTE_ADDR'];;

$query = $con->query("SELECT cod_asignacion_sm FROM ASIG_SAL_MUL,SALA WHERE ASIG_SAL_MUL.cod_sala = SALA.cod_sala AND SALA.cod_sala=".$sala.";");
$row = mysqli_fetch_array($query);
$asignacionDAO = new AsignacionPHSDAO($con);
$asignacionEditado = new AsignacionPHS($codigo,$row[0],$pelicula,$horario);
$asignacionDAO->modificarAsignacionPHS($asignacionEditado);

$auditoriaDAO = new AuditoriaDAO($con);
$nuevaAuditoria = new Auditoria($_SESSION['codigo'],0,$_SESSION['nombre_admin'], 'U',$fecha_auditoria,$ip);
$auditoriaDAO->crearAuditoria($nuevaAuditoria);


?>
<meta http-equiv="refresh" content="0; url=asignaciones.php" />
</html>
