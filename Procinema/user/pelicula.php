<?php
session_start();
require_once('../daos/T_GeneroDAO.php');
require_once('../daos/PeliculaDAO.php');
require_once('../daos/IdiomaDAO.php');
require_once('../daos/MultiplexDAO.php');
require_once('../daos/AsignacionPHSDAO.php');
require_once('../daos/HorarioDAO.php');
require_once('../daos/AsientoDAO.php');
require_once('../util/Conexion.php');
$conexion = new Conexion();
$con = $conexion->conectarBD();

$PeliculaDAO = new PeliculaDAO($con);
$T_GeneroDAO = new T_GeneroDAO($con);
$Multiplex = new MultiplexDAO($con);
$Idioma = new IdiomaDAO($con);
$AsignacionPHSDAO = new AsignacionPHSDAO($con);
$HorarioDAO = new HorarioDAO($con);

$codigo;
$imagen;

if (isset($_GET['pelicula'])) {
    if (isset($_GET['genero'])) {
        if (isset($_GET['idioma'])) {
            $codigo = (int) $_GET['pelicula'];
            $g = (int) $_GET['genero'];
            $idioma = (int) $_GET['idioma'];
            if ($codigo == 1) {
                $imagen = "../rscUser/img/covers/jojo-rabbit.jpg";
            } else if ($codigo == 2) {
                $imagen = "../rscUser/img/covers/sonic.jpg";
            } else if ($codigo == 3) {
                $imagen = "../rscUser/img/covers/1917.jpg";
            } else if ($codigo == 4) {
                $imagen = "../rscUser/img/covers/El-Hombre-Invisible.jpg";
            } else if ($codigo == 5) {
                $imagen = "../rscUser/img/covers/el-oficial-espia.jpg";
            } else if ($codigo == 6) {
                $imagen = "../rscUser/img/covers/Bloodshot.jpg";
            }
        }
    }
} else {
    $codigo = 0;
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Font -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600%7CUbuntu:300,400,500,700" rel="stylesheet">

    <!-- CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="../rscUser/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="../rscUser/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="../rscUser/css/owl.carousel.min.css">
    <link rel="stylesheet" href="../rscUser/css/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="../rscUser/css/nouislider.min.css">
    <link rel="stylesheet" href="../rscUser/css/ionicons.min.css">
    <link rel="stylesheet" href="../rscUser/css/plyr.css">
    <link rel="stylesheet" href="../rscUser/css/photoswipe.css">
    <link rel="stylesheet" href="../rscUser/css/default-skin.css">
    <link rel="stylesheet" href="../rscUser/css/main.css">
    <link rel="stylesheet" href="../rscUser/css/styless.css">

    <!-- Favicons -->
    <link rel="icon" type="../rscUser/image/png" href="../rscUser/icon/logoC.png" sizes="32x32">
    <link rel="apple-touch-icon" href="../rscUser/icon/favicon-32x32.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../rscUser/icon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../rscUser/icon/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="144x144" href="../rscUser/icon/apple-touch-icon-144x144.png">

    <meta name="description" content="">
    <meta name="keywords" content="">
    <title>Procinema - Película</title>

    <style>
        .noc:hover {
            color: antiquewhite;
        }
    </style>

</head>

<body class="body">

    <!-- header -->
    <header class="header">
        <div class="header__wrap">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="header__content">
                            <!-- header logo -->
                            <a href="index.php" class="header__logo">
                                <img src="../rscUser/img/logo.png" alt="">
                            </a>
                            <!-- end header logo -->

                            <ul class="header__nav">
                                <!-- dropdown -->
                                <li class="header__nav-item">
                                    <a href="index.php" class="header__nav-link">Inicio</a>
                                </li>
                                <!-- end dropdown -->
                            </ul>

                            <!-- header auth -->
                            <div class="header__auth">
                                <a href="../logout.php" class="header__sign-in">
                                    <i class="icon ion-ios-log-in"></i>
                                    <span>Cerrar sesión</span>
                                </a>
                            </div>
                            <!-- end header auth -->

                            <!-- header menu btn -->
                            <button class="header__btn" type="button">
                                <span></span>
                                <span></span>
                                <span></span>
                            </button>
                            <!-- end header menu btn -->
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- header search -->
        <form action="#" class="header__search">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="header__search-content">
                            <input type="text" placeholder="Escribe el nombre de la pelicula que quieras comprar">

                            <button type="button">Buscar</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- end header search -->
    </header>
    <!-- end header -->

    <!-- details -->
    <section class="section details">
        <!-- details background -->
        <div class="details__bg" data-bg="../rscUser/img/home/home__bg.jpg"></div>
        <!-- end details background -->

        <!-- details content -->
        <div class="container">
            <div class="row">
                <!-- title -->
                <div class="col-12">
                    <?php $pelicula = $PeliculaDAO->consultarPelicula($codigo) ?>
                    <h1 class="details__title"><?php echo $pelicula->getNom_pelicula() ?></h1>
                </div>
                <!-- end title -->

                <!-- content -->
                <div class="col-12 col-xl-6">
                    <div class="card card--details" style="background:transparent; border: none;">
                        <div class="row">
                            <!-- card cover -->
                            <div class="col-12 col-xl-5">
                                <div class="card__cover">
                                    <img src="<?php echo $imagen ?>" alt="">
                                </div>
                            </div>
                            <!-- end card cover -->

                            <!-- card content -->
                            <div class="col-12 col-xl-7">
                                <div class="card__content">
                                    <div class="card__wrap">
                                        <?php $cal = (string) $pelicula->getCalificacion() ?>
                                        <span class="card__rate"><i class="icon ion-ios-star"></i> <?php echo $cal ?></span>

                                        <ul class="card__list">
                                            <li>10+</li>
                                        </ul>
                                    </div>

                                    <ul class="card__meta">
                                        <?php
                                        $fecha = (string) $pelicula->getFecha();
                                        $duracion = (string) $pelicula->getDuracion();
                                        $genero = $T_GeneroDAO->consultarGenero($g)->getNom_genero();
                                        ?>
                                        <li><span>Genero:</span> <a href="#"><?php echo $genero ?></a></li>

                                        <li><span>Año de lanzamiento:</span> <?php echo $fecha ?></li>
                                        <li><span>Duración:</span> <?php echo $duracion ?> min</li>
                                        <li><span>País:</span> <a href="#">USA</a> </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- end card content -->
                        </div>
                    </div>
                </div>
                <!-- end content -->

                <!-- player -->
                <div class="col-12 col-xl-6">
                    <div class="inputForm">

                        <div class="card__description">

                            <div class="accordion" id="accordion">
                                <div class="accordion__card">
                                    <div class="card-header2" id="headingOne">
                                        <button type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                            <?php
                                            $multiplex = $Multiplex->consultarMultiplex(1)->getNom_multiplex();
                                            ?>
                                            <span>Múltiplex: <?php echo $multiplex ?></span>
                                            <?php
                                            $idiomaPelicula = $Idioma->consultarIdioma($idioma)->getNom_Idioma();
                                            ?>
                                            <span>2D - Hablada en <?php echo $idiomaPelicula ?></span>
                                        </button>
                                    </div>

                                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                        <div class="card-body">
                                            <table class="accordion__list">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 20px;">#</th>
                                                        <th>Horario</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    <?php

                                                    $numero = $AsignacionPHSDAO->obtenerLista();
                                                    $asignacion = $AsignacionPHSDAO->listaAsignacionPelicula(0, 0);
                                                    $asignacion2 = $AsignacionPHSDAO->listaAsignacionPelicula(0, 0);
                                                    $asignacion3 = $AsignacionPHSDAO->listaAsignacionPelicula(0, 0);
                                                    $asignacion4 = $AsignacionPHSDAO->listaAsignacionPelicula(0, 0);
                                                    $asignacion5 = $AsignacionPHSDAO->listaAsignacionPelicula(0, 0);
                                                    $asignacion6 = $AsignacionPHSDAO->listaAsignacionPelicula(0, 0);
                                                    $asig = 0;

                                                    foreach ($numero as $lista) {

                                                        $noc = $lista->getCod_asignacion_sm();
                                                        $sic = $lista->getCod_Pelicula();
                                                        $asig = $lista->getCod_asignacion();

                                                        if (($noc == 1 || $noc == 2 || $noc == 3) && ($codigo == $sic)) {
                                                            $asignacion = $AsignacionPHSDAO->listaAsignacionPelicula($noc, $codigo);
                                                        }

                                                        if (($noc == 4 || $noc == 6 || $noc == 7) && ($codigo == $sic)) {
                                                            $asignacion2 = $AsignacionPHSDAO->listaAsignacionPelicula($noc, $codigo);
                                                        }

                                                        if (($noc == 8 || $noc == 9 || $noc == 10) && ($codigo == $sic)) {
                                                            $asignacion3 = $AsignacionPHSDAO->listaAsignacionPelicula($noc, $codigo);
                                                        }

                                                        if (($noc == 11 || $noc == 12 || $noc == 13) && ($codigo == $sic)) {
                                                            $asignacion4 = $AsignacionPHSDAO->listaAsignacionPelicula($noc, $codigo);
                                                        }

                                                        if (($noc == 14 || $noc == 15 || $noc == 16) && ($codigo == $sic)) {
                                                            $asignacion5 = $AsignacionPHSDAO->listaAsignacionPelicula($noc, $codigo);
                                                        }

                                                        if (($noc == 17 || $noc == 18 || $noc == 19) && ($codigo == $sic)) {
                                                            $asignacion6 = $AsignacionPHSDAO->listaAsignacionPelicula($noc, $codigo);
                                                        }
                                                    }

                                                    foreach ($asignacion as $consecutivo => $hora) {

                                                        $horario = $HorarioDAO->consultarHorario($hora->getCod_Horario())->getNom_horario();
                                                        $seleccionado = $AsignacionPHSDAO->consultarAsignacionPHS($hora->getCod_asignacion())->getCod_asignacion();

                                                    ?>
                                                        <tr>

                                                            <th><?php echo $consecutivo + 1 ?></th>
                                                            <td>
                                                                <buuton class="noc"><?php echo $horario; ?></buuton>
                                                            </td>
                                                            <td>
                                                                <input type="radio" name="seleccion" value="<?php echo $seleccionado; ?> ">
                                                            </td>
                                                        </tr>
                                                    <?php
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="accordion__card">
                                    <div class="card-header2" id="headingTwo">
                                        <button class="collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            <?php
                                            $multiplex2 = $Multiplex->consultarMultiplex(2)->getNom_multiplex();
                                            ?>
                                            <span>Múltiplex: <?php echo $multiplex2 ?></span>
                                            <span>2D - Hablada en <?php echo $idiomaPelicula ?></span>
                                        </button>
                                    </div>

                                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                        <div class="card-body">
                                            <table class="accordion__list">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 20px;">#</th>
                                                        <th>Horario</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    <?php

                                                    foreach ($asignacion2 as $consecutivo2 => $hora) {
                                                        $horario2 = $HorarioDAO->consultarHorario($hora->getCod_Horario())->getNom_horario();
                                                        $seleccionado2 = $AsignacionPHSDAO->consultarAsignacionPHS($hora->getCod_asignacion())->getCod_asignacion();

                                                    ?>
                                                        <tr>

                                                            <th><?php echo $consecutivo2 + 1 ?></th>
                                                            <td>
                                                                <buuton class="noc"><?php echo $horario2; ?></buuton>
                                                            </td>
                                                            <td>
                                                                <input type="radio" name="seleccion" value="<?php echo $seleccionado2; ?> ">
                                                            </td>
                                                        </tr>
                                                    <?php
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="accordion__card">
                                    <div class="card-header2" id="headingThree">
                                        <button class="collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            <?php
                                            $multiplex3 = $Multiplex->consultarMultiplex(3)->getNom_multiplex();
                                            ?>
                                            <span>Múltiplex: <?php echo $multiplex3 ?></span>
                                            <span>2D - Hablada en <?php echo $idiomaPelicula ?></span>
                                        </button>
                                    </div>

                                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                            <table class="accordion__list">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 20px;">#</th>
                                                        <th>Horario</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    <?php

                                                    foreach ($asignacion3 as $consecutivo3 => $hora) {
                                                        $horario3 = $HorarioDAO->consultarHorario($hora->getCod_Horario())->getNom_horario();
                                                        $seleccionado3 = $AsignacionPHSDAO->consultarAsignacionPHS($hora->getCod_asignacion())->getCod_asignacion();

                                                    ?>
                                                        <tr>

                                                            <th><?php echo $consecutivo3 + 1 ?></th>
                                                            <td>
                                                                <buuton class="noc"><?php echo $horario3; ?></buuton>
                                                            </td>
                                                            <td>
                                                                <input type="radio" name="seleccion" value="<?php echo $seleccionado3; ?> ">
                                                            </td>
                                                        </tr>
                                                    <?php
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="accordion__card">
                                    <div class="card-header2" id="headingFour">
                                        <button class="collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                            <?php
                                            $multiplex4 = $Multiplex->consultarMultiplex(4)->getNom_multiplex();
                                            ?>
                                            <span>Múltiplex: <?php echo $multiplex4 ?></span>
                                            <span>2D - Hablada en <?php echo $idiomaPelicula ?></span>
                                        </button>
                                    </div>

                                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                                        <div class="card-body">
                                            <table class="accordion__list">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 20px;">#</th>
                                                        <th>Horario</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    <?php

                                                    foreach ($asignacion4 as $consecutivo4 => $hora) {
                                                        $horario4 = $HorarioDAO->consultarHorario($hora->getCod_Horario())->getNom_horario();
                                                        $seleccionado4 = $AsignacionPHSDAO->consultarAsignacionPHS($hora->getCod_asignacion())->getCod_asignacion();

                                                    ?>
                                                        <tr>

                                                            <th><?php echo $consecutivo4 + 1 ?></th>
                                                            <td>
                                                                <buuton class="noc"><?php echo $horario4; ?></buuton>
                                                            </td>
                                                            <td>
                                                                <input type="radio" name="seleccion" value="<?php echo $seleccionado4; ?> ">
                                                            </td>
                                                        </tr>
                                                    <?php
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="accordion__card">
                                    <div class="card-header2" id="headingFive">
                                        <button class="collapsed" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                            <?php
                                            $multiplex5 = $Multiplex->consultarMultiplex(5)->getNom_multiplex();
                                            ?>
                                            <span>Múltiplex: <?php echo $multiplex5 ?></span>
                                            <span>2D - Hablada en <?php echo $idiomaPelicula ?></span>
                                        </button>
                                    </div>

                                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                                        <div class="card-body">
                                            <table class="accordion__list">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 20px;">#</th>
                                                        <th>Horario</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php

                                                    foreach ($asignacion5 as $consecutivo5 => $hora) {
                                                        $horario5 = $HorarioDAO->consultarHorario($hora->getCod_Horario())->getNom_horario();
                                                        $seleccionado5 = $AsignacionPHSDAO->consultarAsignacionPHS($hora->getCod_asignacion())->getCod_asignacion();

                                                    ?>
                                                        <tr>

                                                            <th><?php echo $consecutivo5 + 1 ?></th>
                                                            <td>
                                                                <buuton class="noc"><?php echo $horario5; ?></buuton>
                                                            </td>
                                                            <td>
                                                                <input type="radio" name="seleccion" value="<?php echo $seleccionado5; ?> ">
                                                            </td>
                                                        </tr>
                                                    <?php
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="accordion__card">
                                    <div class="card-header2" id="headingSix">
                                        <button class="collapsed" type="button" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                            <?php
                                            $multiplex6 = $Multiplex->consultarMultiplex(6)->getNom_multiplex();
                                            ?>
                                            <span>Múltiplex: <?php echo $multiplex6 ?></span>
                                            <span>2D - Hablada en <?php echo $idiomaPelicula ?></span>
                                        </button>
                                    </div>

                                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
                                        <div class="card-body">
                                            <table class="accordion__list">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 20px;">#</th>
                                                        <th>Horario</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    <?php

                                                    foreach ($asignacion6 as $consecutivo6 => $hora) {
                                                        $horario6 = $HorarioDAO->consultarHorario($hora->getCod_Horario())->getNom_horario();
                                                        $seleccionado6 = $AsignacionPHSDAO->consultarAsignacionPHS($hora->getCod_asignacion())->getCod_asignacion();

                                                    ?>
                                                        <tr>

                                                            <th><?php echo $consecutivo6 + 1 ?></th>
                                                            <td>
                                                                <buuton class="noc"><?php echo $horario6; ?></buuton>
                                                            </td>
                                                            <td>
                                                                <input type="radio" name="seleccion" value="<?php echo $seleccionado6; ?> ">
                                                            </td>
                                                        </tr>
                                                    <?php
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
                <!-- end player -->
            </div>
        </div>
        <!-- end details content -->
    </section>

    <div class="container">
        <div class="row">

            <!-- input fields -->
            <div class="col-12 col-xl-7"><br><br>
                <div class="inputForm">
                    <div class="col-12 col-xl-10">
                        <div class="caja" id="accordion">
                            <div class="accordion__card">
                                <div class="card-header" id="headingOne">
                                    <h1 class="holaaa">Por favor ingrese los siguientes
                                        datos
                                        para la compra:</h1>
                                    <div class="mr_agilemain">
                                        <div class="agileits-left">
                                            <label class="prueba"> Código cliente</label><span style="color: #A3A3A3">*</span>
                                            <input type="number" id="Username" name="Username" value="<?php echo $_SESSION['cod_cli']; ?>" desable required>
                                        </div>
                                        <div class="agileits-right">
                                            <label class="prueba"> Número de asientos</label><span style="color: #A3A3A3">*</span>
                                            <input type="number" id="Numseats" min="1" max="110" required>
                                        </div>
                                    </div>
                                    <div class="mr_agilemain">
                                        <div class="agileits-left">
                                            <label class="prueba"> Localidad</label><span style="color: #A3A3A3">*</span><br>
                                            <input type="radio" id="general" name="gender" value="0" checked>
                                            <label for="general" style="font-style: italic;">Compra
                                                general</label><br>
                                            <input type="radio" id="preferencial" name="gender" value="1">
                                            <label for="preferencial">Compra preferencial</label>
                                        </div>
                                        <div class="agileits-right">
                                            <label class="prueba"> Servicio por boleta</label><br>
                                            <label style="color: #A3A3A3">$11.000</label><br>
                                            <label style="color: #A3A3A3">$15.000</label>
                                        </div>
                                    </div>
                                </div>
                                <center>
                                    <br><button onclick="takeData()" class="boton">Aceptar</button>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-xl-5">
                <!-- //input fields -->
                <!-- seat availabilty list -->
                <ul class="seat_w3ls">
                    <li class="smallBox greenBox">Asientos seleccionados</li>

                    <li class="smallBox redBox">Asientos reservados</li>
                    <center>
                        <li class="smallBox emptyBox">Asientos disponibles</li>
                    </center>
                </ul>
                <!-- seat availabilty list -->
                <!-- seat layout -->
                <div class="seatStructure txt-center" style="overflow-x:auto;">
                    <table id="seatsBlock">
                        <p id="notification"></p>
                        <tr>
                            <td></td>
                            <td>1</td>
                            <td>2</td>
                            <td>3</td>
                            <td>4</td>
                            <td>5</td>
                            <td></td>
                            <td>6</td>
                            <td>7</td>
                            <td>8</td>
                            <td>9</td>
                            <td>10</td>
                            <td>11</td>
                            <td>12</td>
                        </tr>
                        <tr>
                            <td>A</td>

                            <?php
                                $asientosDAO = new AsientoDAO($con);
                                $asientoss = $asientosDAO->listaA5();

                                foreach ($asientoss as $asiento){
                            ?>

                            <td>
                                <input type="checkbox" class="seats pruebaaa" name="preferencial" value="<?php echo $asiento->getCod_Asiento(); ?>">
                            </td>
                            <?php
                                }
                            ?>
                            <td></td>
                            <?php
                                $asientoss2 = $asientosDAO->listaA7();

                                foreach ($asientoss2 as $asiento2){
                            ?>

                            <td>
                                <input type="checkbox" class="seats pruebaaa" name="preferencial" value="<?php echo $asiento2->getCod_Asiento(); ?>">
                            </td>
                            <?php
                                }
                            ?>
                        </tr>

                        <tr>
                            <td>B</td>
                            <?php
                                $asientoss3 = $asientosDAO->listaB5();

                                foreach ($asientoss3 as $asiento3){
                            ?>

                            <td>
                                <input type="checkbox" class="seats pruebaaa" name="preferencial" value="<?php echo $asiento3->getCod_Asiento(); ?>">
                            </td>
                            <?php
                                }
                            ?>
                            <td></td>
                            <?php
                                $asientoss4 = $asientosDAO->listaB7();

                                foreach ($asientoss4 as $asiento4){
                            ?>

                            <td>
                                <input type="checkbox" class="seats pruebaaa" name="preferencial" value="<?php echo $asiento4->getCod_Asiento(); ?>">
                            </td>
                            <?php
                                }
                            ?>
                        </tr>

                        <tr>
                            <td>C</td>
                            <?php
                                $asientoss5 = $asientosDAO->listaC5();

                                foreach ($asientoss5 as $asiento5){
                            ?>

                            <td>
                                <input type="checkbox" class="seats" name="normal" value="<?php echo $asiento5->getCod_Asiento(); ?>">
                            </td>
                            <?php
                                }
                            ?>
                            <td></td>
                            <?php
                                $asientoss6 = $asientosDAO->listaC7();

                                foreach ($asientoss6 as $asiento6){
                            ?>

                            <td>
                                <input type="checkbox" class="seats" name="normal" value="<?php echo $asiento6->getCod_Asiento(); ?>">
                            </td>
                            <?php
                                }
                            ?>
                        </tr>

                        <tr>
                            <td>D</td>
                            <?php
                                $asientoss7 = $asientosDAO->listaD5();

                                foreach ($asientoss7 as $asiento7){
                            ?>

                            <td>
                                <input type="checkbox" class="seats" name="normal" value="<?php echo $asiento7->getCod_Asiento(); ?>">
                            </td>
                            <?php
                                }
                            ?>
                            <td></td>
                            <?php
                                $asientoss8 = $asientosDAO->listaD7();

                                foreach ($asientoss8 as $asiento8){
                            ?>

                            <td>
                                <input type="checkbox" class="seats" name="normal" value="<?php echo $asiento8->getCod_Asiento(); ?>">
                            </td>
                            <?php
                                }
                            ?>
                        </tr>

                        <tr>
                            <td>E</td>
                            <?php
                                $asientoss9 = $asientosDAO->listaE5();

                                foreach ($asientoss9 as $asiento9){
                            ?>

                            <td>
                                <input type="checkbox" class="seats" name="normal" value="<?php echo $asiento9->getCod_Asiento(); ?>">
                            </td>
                            <?php
                                }
                            ?>
                            <td></td>
                            <?php
                                $asientoss10 = $asientosDAO->listaE7();

                                foreach ($asientoss10 as $asiento10){
                            ?>

                            <td>
                                <input type="checkbox" class="seats" name="normal" value="<?php echo $asiento10->getCod_Asiento(); ?>">
                            </td>
                            <?php
                                }
                            ?>
                        </tr>

                        <tr class="seatVGap"></tr>

                        <tr>
                            <td>F</td>
                            <?php
                                $asientoss11 = $asientosDAO->listaF5();

                                foreach ($asientoss11 as $asiento11){
                            ?>

                            <td>
                                <input type="checkbox" class="seats" name="normal" value="<?php echo $asiento11->getCod_Asiento(); ?>">
                            </td>
                            <?php
                                }
                            ?>
                            <td></td>
                            <?php
                                $asientoss12 = $asientosDAO->listaF7();

                                foreach ($asientoss12 as $asiento12){
                            ?>

                            <td>
                                <input type="checkbox" class="seats" name="normal" value="<?php echo $asiento12->getCod_Asiento(); ?>">
                            </td>
                            <?php
                                }
                            ?>
                        </tr>

                        <tr>
                            <td>G</td>
                            <?php
                                $asientoss13 = $asientosDAO->listaG5();

                                foreach ($asientoss13 as $asiento13){
                            ?>

                            <td>
                                <input type="checkbox" class="seats" name="normal" value="<?php echo $asiento13->getCod_Asiento(); ?>">
                            </td>
                            <?php
                                }
                            ?>
                            <td></td>
                            <?php
                                $asientoss14 = $asientosDAO->listaG7();

                                foreach ($asientoss14 as $asiento14){
                            ?>

                            <td>
                                <input type="checkbox" class="seats" name="normal" value="<?php echo $asiento14->getCod_Asiento(); ?>">
                            </td>
                            <?php
                                }
                            ?>
                        </tr>

                        <tr>
                            <td>H</td>
                            <?php
                                $asientoss15 = $asientosDAO->listaH5();

                                foreach ($asientoss15 as $asiento15){
                            ?>

                            <td>
                                <input type="checkbox" class="seats" name="normal" value="<?php echo $asiento15->getCod_Asiento(); ?>">
                            </td>
                            <?php
                                }
                            ?>
                            <td></td>
                            <?php
                                $asientoss16 = $asientosDAO->listaH7();

                                foreach ($asientoss16 as $asiento16){
                            ?>

                            <td>
                                <input type="checkbox" class="seats" name="normal" value="<?php echo $asiento16->getCod_Asiento(); ?>">
                            </td>
                            <?php
                                }
                            ?>
                        </tr>

                        <tr>
                            <td>I</td>
                            <?php
                                $asientoss17 = $asientosDAO->listaI5();

                                foreach ($asientoss17 as $asiento17){
                            ?>

                            <td>
                                <input type="checkbox" class="seats" name="normal" value="<?php echo $asiento17->getCod_Asiento(); ?>">
                            </td>
                            <?php
                                }
                            ?>
                            <td></td>
                            <?php
                                $asientoss18 = $asientosDAO->listaI7();

                                foreach ($asientoss18 as $asiento18){
                            ?>

                            <td>
                                <input type="checkbox" class="seats" name="normal" value="<?php echo $asiento18->getCod_Asiento(); ?>">
                            </td>
                            <?php
                                }
                            ?>
                        </tr>

                        <tr>
                            <td>J</td>
                            <?php
                                $asientoss19 = $asientosDAO->listaJ5();

                                foreach ($asientoss19 as $asiento19){
                            ?>

                            <td>
                                <input type="checkbox" class="seats" name="normal" value="<?php echo $asiento19->getCod_Asiento(); ?>">
                            </td>
                            <?php
                                }
                            ?>
                            <td></td>
                            <?php
                                $asientoss20 = $asientosDAO->listaJ7();

                                foreach ($asientoss20 as $asiento20){
                            ?>

                            <td>
                                <input type="checkbox" class="seats" name="normal" value="<?php echo $asiento20->getCod_Asiento(); ?>">
                            </td>
                            <?php
                                }
                            ?>
                        </tr>
                    </table>

                    <div class="screen">
                        <h2 class="wthree">Pantalla</h2>
                    </div>
                    <center><br>
                        <button onclick="updateTextArea()" class="boton2">Confirmar</button></center><br>
                </div>
                <!-- //seat layout -->
            </div>
        </div>
    </div>
    <!--
	<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
		Launch demo modal
	</button> -->

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ingresar tarjeta</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="#" method="GET" name="formulario" id="formulario">
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label" style="color: black;">Número de tarjeta:</label>
                            <input type="number" class="form-control" id="recipient-name" style="width: 100%;" placeholder="Tarjeta">
                        </div>
                        <div class="modal-footer d-flex justify-content-center">
                            <input type="submit" value="Aceptar" class="btn btn-dark">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- end details -->

    <!-- Root element of PhotoSwipe. Must have class pswp. -->
    <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

        <!-- Background of PhotoSwipe.
		It's a separate element, as animating opacity is faster than rgba(). -->
        <div class="pswp__bg"></div>

        <!-- Slides wrapper with overflow:hidden. -->
        <div class="pswp__scroll-wrap">

            <!-- Container that holds slides. PhotoSwipe keeps only 3 slides in DOM to save memory. -->
            <!-- don't modify these 3 pswp__item elements, data is added later on. -->
            <div class="pswp__container">
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
            </div>

            <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
            <div class="pswp__ui pswp__ui--hidden">

                <div class="pswp__top-bar">

                    <!--  Controls are self-explanatory. Order can be changed. -->

                    <div class="pswp__counter"></div>

                    <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                    <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                    <!-- Preloader -->
                    <div class="pswp__preloader">
                        <div class="pswp__preloader__icn">
                            <div class="pswp__preloader__cut">
                                <div class="pswp__preloader__donut"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>

                <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>

                <div class="pswp__caption">
                    <div class="pswp__caption__center"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- JS -->
    <script src="../rscUser/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="https://checkout.epayco.co/checkout.js"></script>
    <script>
        document.getElementById("Username").disabled = true;
        var love;

        function comprobar9() {
            if (document.querySelector('input[name="seleccion"]:checked')) {
                love = $('input:radio[name=seleccion]:checked').val();
                return compraa(love);
            } else {
                alert("Error, seleccione un horario");
            }
        }

        var valor;
        var asient;
        var sillas=[];

        function prueba(precio, asientos) {
            valor = precio;
            asient = asientos;
            return valor, asientos;
        }

        function asientos(aver) {
            aver.forEach(function (elemento, indice, array) {
                sillas.push(elemento);
            });
            // f = sillas[sillas.length-1];
            // alert(f);
            return sillas;
        }

        function compraa(horarioo) {

            var handler = ePayco.checkout.configure({
                key: '45b960805ced5c27ce34b1600b4b9f54',
                test: false
            })

            var data = {
                //Parametros compra (obligatorio)
                name: asient.toString() + " asientos, Procinema.",
                description: asient.toString() + " asientos, Procinema.",
                currency: "cop",
                amount: valor.toString(),
                tax_base: "0",
                tax: "0",
                country: "co",
                lang: "es",
                //Onpage="false" - Standard="true"
                external: "false",
                extra1: sillas,
                extra2: asient.toString(),
                extra3: horarioo.toString(),
                confirmation: "http://ec2-34-236-135-239.compute-1.amazonaws.com/procinema/procinema/user/respuesta-pelicula.php",
                response: "http://ec2-34-236-135-239.compute-1.amazonaws.com/procinema/procinema/user/respuesta-pelicula.php",
                // confirmation: "http://localhost:82/Ing2/procinema/Procinema/user/respuesta-pelicula.php",
                // response: "http://localhost:82/Ing2/procinema/Procinema/user/respuesta-pelicula.php"
            }

            handler.open(data);

        }

        function onLoaderFunc() {
            $(".seatStructure *").prop("disabled", true);
            $(".displayerBoxes *").prop("disabled", true);
        }

        function takeData() {
            if (document.querySelector('input[name="seleccion"]:checked')) {
                if (($("#Username").val().length == 0) || ($("#Numseats").val().length == 0) || ($("#Numseats").val() == 0)) {

                    alert("Por favor ingrese la cédula y la cantidad de asientos");

                } else {
                    if ($("#Numseats").val() > 120) {
                        alert("El maximo de asientos es 110");
                    } else {
                        //  $(".inputForm *").prop("disabled", true);
                        $(".seatStructure *").prop("disabled", false);
                        document.getElementById("notification").innerHTML =
                            "<b style='margin-bottom:0px;background:rgb(248, 210, 194);letter-spacing:1px;'>Por favor selecciona los asientos!</b>";
                        if (document.querySelector('input[name="gender"]:checked')) {

                            var compra = $('input:radio[name=gender]:checked').val();
                            if (compra == 0) {
                                var total = ($("#Numseats").val()) * 11000;
                                var asientos = ($("#Numseats").val());
                                prueba(total, asientos);
                            } else {
                                var total = ($("#Numseats").val()) * 15000;
                                var asientos = ($("#Numseats").val());
                                prueba(total, asientos);
                            }

                        }
                    }
                }
            } else {
                alert("Error, seleccione un horario");
            }
        }


        function updateTextArea() {


            if (($("input:checked").length) - 2 == ($("#Numseats").val())) {

                $(".seatStructure *").prop("disabled", true);

                var allNumberVals = [];
                var allSeatsVals = [];

                allNumberVals.push($("#Numseats").val());
                $('#seatsBlock :checked').each(function() {
                    allSeatsVals.push($(this).val());
                });

                asientos(allSeatsVals);
                comprobar9();

            } else {
                alert("Por favor seleccione " + ($("#Numseats").val()) + " asientos")
            }
        }


        function myFunction() {
            alert($("input:checked").length);
        }

        /*
        function getCookie(cname) {
        	var name = cname + "=";
        	var ca = document.cookie.split(';');
        	for(var i = 0; i < ca.length; i++) {
        		var c = ca[i];
        		while (c.charAt(0) == ' ') {
        			c = c.substring(1);
        		}
        		if (c.indexOf(name) == 0) {
        			return c.substring(name.length, c.length);
        		}
        	}
        	return "";
        }
        */


        $(":checkbox").click(function() {
            if (($("input:checked").length) - 2 == ($("#Numseats").val())) {

                // var check = $('input:checkbox[name=preferencial]:checked');
                // if ($('input:radio[name=gender]:checked').val()==1) {
                // 		if (check) {
                // 				$(":checkbox").prop('disabled', true);
                // 				$(':checked').prop('disabled', false);
                // 			} else {
                // 				$(":checkbox").prop('disabled', false);
                // 			}

                // }
                $(":checkbox").prop('disabled', true);
                $(':checked').prop('disabled', false);
            } else {
                $(":checkbox").prop('disabled', false);
            }
        });
    </script>
    <script src="../rscUser/js/bootstrap.bundle.min.js"></script>
    <script src="../rscUser/js/owl.carousel.min.js"></script>
    <script src="../rscUser/js/jquery.mousewheel.min.js"></script>
    <script src="../rscUser/js/jquery.mCustomScrollbar.min.js"></script>
    <script src="../rscUser/js/wNumb.js"></script>
    <script src="../rscUser/js/nouislider.min.js"></script>
    <script src="../rscUser/js/plyr.min.js"></script>
    <script src="../rscUser/js/jquery.morelines.min.js"></script>
    <script src="../rscUser/js/photoswipe.min.js"></script>
    <script src="../rscUser/js/photoswipe-ui-default.min.js"></script>
    <script src="../rscUser/js/main.js"></script>
</body>

</html>
