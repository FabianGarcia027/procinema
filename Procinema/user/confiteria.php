<?php
session_start();
if($_SESSION['nombre']==null){
    header("Location: ../logout.php");
}

require_once('../daos/ConfiteriaDAO.php');

$confiteria = new ConfiteriaDAO();

if(isset($_GET['f_pago']) && isset($_GET['fecha']) && isset($_GET['cantidad']) && isset($_GET['asignacion']) && isset($_GET['asiento']) && isset($_GET['precio'])){
	$pago = $_GET['f_pago'];
	$fecha = $_GET['fecha'];
	$cantidad = $_GET['cantidad'];
	$asignacion = $_GET['asignacion'];
	$asiento = $_GET['asiento'];
	$precioPelicula = $_GET['precio'];
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Font -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600%7CUbuntu:300,400,500,700" rel="stylesheet">

	<!-- CSS -->
	<link rel="stylesheet" href="../rscUser/css/bootstrap-reboot.min.css">
	<link rel="stylesheet" href="../rscUser/css/bootstrap-grid.min.css">
	<link rel="stylesheet" href="../rscUser/css/owl.carousel.min.css">
	<link rel="stylesheet" href="../rscUser/css/jquery.mCustomScrollbar.min.css">
	<link rel="stylesheet" href="../rscUser/css/nouislider.min.css">
	<link rel="stylesheet" href="../rscUser/css/ionicons.min.css">
	<link rel="stylesheet" href="../rscUser/css/plyr.css">
	<link rel="stylesheet" href="../rscUser/css/photoswipe.css">
	<link rel="stylesheet" href="../rscUser/css/default-skin.css">
	<link rel="stylesheet" href="../rscUser/css/main.css">

	<!-- Favicons -->
	<link rel="icon" type="../rscUser/image/png" href="../rscUser/icon/logoC.png" sizes="32x32">
	<link rel="apple-touch-icon" href="../rscUser/icon/favicon-32x32.png">
	<link rel="apple-touch-icon" sizes="72x72" href="../rscUser/icon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="../rscUser/icon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="144x144" href="../rscUser/icon/apple-touch-icon-144x144.png">

	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="Dmitry Volkov">
	<title>Procinema – Confiteria</title>

</head>
<body class="body">

	<!-- header -->
	<header class="header">
		<div class="header__wrap">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="header__content">
							<!-- header logo -->
							<a href="index.php" class="header__logo">
								<img src="../rscUser/img/logo.png" alt="">
							</a>
							<!-- end header logo -->

							<!-- header auth -->
							<div class="header__auth">
								<button class="header__search-btn" type="button">
									<i class="icon ion-ios-search"></i>
								</button>

								<a href="../signin.php" class="header__sign-in">
									<i class="icon ion-ios-log-in"></i>
									<span>Cerrar Sesión</span>
								</a>
							</div>
							<!-- end header auth -->


						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- header search -->
		<form action="#" class="header__search">
			<div class="container">
				<div class="row">

					<div class="header__search-content">
						<input type="text" placeholder="Buscar producto en Confiteria">

						<button type="button">Buscar</button>
					</div>
				</div>
			</div>
		</form>
		<!-- end header search -->
	</header>
	<!-- end header -->
	<section class="home">
		<!-- home bg -->
		<div class="owl-carousel home__bg">
			<div class="item home__cover" data-bg="../rscUser/img/productos/fondos/1.jpg"></div>
			<div class="item home__cover" data-bg="../rscUser/img/productos/fondos/2.jpg"></div>
			<div class="item home__cover" data-bg="../rscUser/img/productos/fondos/3.jpg"></div>
			<div class="item home__cover" data-bg="../rscUser/img/productos/fondos/4.jpg"></div>
			<div class="item home__cover" data-bg="../rscUser/img/productos/fondos/5.jpg"></div>
			<div class="item home__cover" data-bg="../rscUser/img/productos/fondos/6.jpg"></div>
			<div class="item home__cover" data-bg="../rscUser/img/productos/fondos/7.jpg"></div>

		</div>
		<!-- end home bg -->

		<div class="container">
			<div class="row">
				<div class="col-12">
					<h1 class="home__title"><b>Combos</b></h1>

					<button class="home__nav home__nav--prev" type="button">
						<i class="icon ion-ios-arrow-round-back"></i>
					</button>
					<button class="home__nav home__nav--next" type="button">
						<i class="icon ion-ios-arrow-round-forward"></i>
					</button>
				</div>
				<?php
						$combo1 = $confiteria->consultarConfiteria(1);
						$combo2 = $confiteria->consultarConfiteria(2);
						$combo3 = $confiteria->consultarConfiteria(3);
						$combo4 = $confiteria->consultarConfiteria(4);
						$combo5 = $confiteria->consultarConfiteria(5);
						$combo6 = $confiteria->consultarConfiteria(6);
							?>


				<div class="col-12">
					<div class="owl-carousel home__carousel">
						<div class="item">
							<!-- card -->
							<div class="card card--big">
								<div class="card__cover">
									<img src="../rscUser/img/productos/Combo1.jpg" alt="">
									<a href="respuesta.php?cod_confiteria=<?php echo $combo1->getCod_confiteria() ?>&precio_confiteria=<?php echo $combo1->getPrecio() ?>&pago=<?php echo $pago ?>&fecha=<?php echo $fecha ?>&cantidad=<?php echo $cantidad ?>&asignacion=<?php echo $asignacion ?>&asiento=<?php echo $asiento ?>&precio_pelicula=<?php echo $precioPelicula ?>" class="card__play">
										<i class="icon ion-ios-cart"></i>
									</a>
								</div>
								<div class="card__content">
									<h3 class="card__title"><a href="respuesta.php?cod_confiteria=<?php echo $combo1->getCod_confiteria() ?>&precio_confiteria=<?php echo $combo1->getPrecio() ?>&pago=<?php echo $pago ?>&fecha=<?php echo $fecha ?>&cantidad=<?php echo $cantidad ?>&asignacion=<?php echo $asignacion ?>&asiento=<?php echo $asiento ?>&precio_pelicula=<?php echo $precioPelicula ?>"></a></h3>
									<span class="card__category" style="color: white; font-size: 14px">Descripción: <?php echo  $combo1->getDescripcion() ?></span>
									<span class="card__rate"><i class="icon ion-ios-pricetags"></i>$<?php echo $combo1->getPrecio() ?></span>
								</div>

							</div>
							<!-- end card -->

						</div>

						<div class="item">
							<!-- card -->
							<div class="card card--big">
								<div class="card card--big">
									<div class="card__cover">
										<img src="../rscUser/img/productos/Combo2.jpg" alt="">
										<a href="respuesta.php?cod_confiteria=<?php echo $combo2->getCod_confiteria() ?>&precio_confiteria=<?php echo $combo2->getPrecio() ?>&pago=<?php echo $pago ?>&fecha=<?php echo $fecha ?>&cantidad=<?php echo $cantidad ?>&asignacion=<?php echo $asignacion ?>&asiento=<?php echo $asiento ?>&precio_pelicula=<?php echo $precioPelicula ?>" class="card__play">
											<i class="icon ion-ios-cart"></i>
										</a>
									</div>
									<div class="card__content">
										<h3 class="card__title"><a href="respuesta.php?cod_confiteria=<?php echo $combo2->getCod_confiteria() ?>&precio_confiteria=<?php echo $combo2->getPrecio() ?>&pago=<?php echo $pago ?>&fecha=<?php echo $fecha ?>&cantidad=<?php echo $cantidad ?>&asignacion=<?php echo $asignacion ?>&asiento=<?php echo $asiento ?>&precio_pelicula=<?php echo $precioPelicula ?>"></a></h3>
										<span class="card__category" style="color: white; font-size: 14px">Descripción: <?php echo  $combo2->getDescripcion() ?></span>
										<span class="card__rate"><i class="icon ion-ios-pricetags"></i>$<?php echo $combo2->getPrecio() ?></span>
									</div>

								</div>


							</div>
							<!-- end card -->
						</div>


						<div class="item">
							<!-- card -->
							<div class="card card--big">
								<div class="card card--big">
									<div class="card__cover">
										<img src="../rscUser/img/productos/Combo3.jpg" alt="">
										<a href="respuesta.php?cod_confiteria=<?php echo $combo3->getCod_confiteria() ?>&precio_confiteria=<?php echo $combo3->getPrecio() ?>&pago=<?php echo $pago ?>&fecha=<?php echo $fecha ?>&cantidad=<?php echo $cantidad ?>&asignacion=<?php echo $asignacion ?>&asiento=<?php echo $asiento ?>&precio_pelicula=<?php echo $precioPelicula ?>" class="card__play">
											<i class="icon ion-ios-cart"></i>
										</a>
									</div>
									<div class="card__content">
										<h3 class="card__title"><a href="respuesta.php?cod_confiteria=<?php echo $combo3->getCod_confiteria() ?>&precio_confiteria=<?php echo $combo3->getPrecio() ?>&pago=<?php echo $pago ?>&fecha=<?php echo $fecha ?>&cantidad=<?php echo $cantidad ?>&asignacion=<?php echo $asignacion ?>&asiento=<?php echo $asiento ?>&precio_pelicula=<?php echo $precioPelicula ?>"></a></h3>
										<span class="card__category" style="color: white; font-size: 14px">Descripción: <?php echo  $combo3->getDescripcion() ?></span>
										<span class="card__rate"><i class="icon ion-ios-pricetags"></i>$<?php echo $combo3->getPrecio() ?></span>
									</div>

								</div>


							</div>
							<!-- end card -->
						</div>
						<div class="item">
							<!-- card -->
							<div class="card card--big">
								<div class="card card--big">
									<div class="card__cover">
										<img src="../rscUser/img/productos/Combo4.jpg" alt="">
										<a href="respuesta.php?cod_confiteria=<?php echo $combo4->getCod_confiteria() ?>&precio_confiteria=<?php echo $combo4->getPrecio() ?>&pago=<?php echo $pago ?>&fecha=<?php echo $fecha ?>&cantidad=<?php echo $cantidad ?>&asignacion=<?php echo $asignacion ?>&asiento=<?php echo $asiento ?>&precio_pelicula=<?php echo $precioPelicula ?>" class="card__play">
											<i class="icon ion-ios-cart"></i>
										</a>
									</div>
									<div class="card__content">
										<h3 class="card__title"><a href="respuesta.php?cod_confiteria=<?php echo $combo4->getCod_confiteria() ?>&precio_confiteria=<?php echo $combo4->getPrecio() ?>&pago=<?php echo $pago ?>&fecha=<?php echo $fecha ?>&cantidad=<?php echo $cantidad ?>&asignacion=<?php echo $asignacion ?>&asiento=<?php echo $asiento ?>&precio_pelicula=<?php echo $precioPelicula ?>"></a></h3>
										<span class="card__category" style="color: white; font-size: 14px">Descripción: <?php echo  $combo4->getDescripcion() ?></span>
										<span class="card__rate"><i class="icon ion-ios-pricetags"></i>$<?php echo $combo4->getPrecio() ?></span>
									</div>

								</div>


							</div>
							<!-- end card -->
						</div>
						<div class="item">
							<!-- card -->
							<div class="card card--big">
								<div class="card card--big">
									<div class="card__cover">
										<img src="../rscUser/img/productos/Combo5.jpg" alt="">
										<a href="respuesta.php?cod_confiteria=<?php echo $combo5->getCod_confiteria() ?>&precio_confiteria=<?php echo $combo5->getPrecio() ?>&pago=<?php echo $pago ?>&fecha=<?php echo $fecha ?>&cantidad=<?php echo $cantidad ?>&asignacion=<?php echo $asignacion ?>&asiento=<?php echo $asiento ?>&precio_pelicula=<?php echo $precioPelicula ?>" class="card__play">
											<i class="icon ion-ios-cart"></i>
										</a>
									</div>
									<div class="card__content">
										<h3 class="card__title"><a href="respuesta.php?cod_confiteria=<?php echo $combo5->getCod_confiteria() ?>&precio_confiteria=<?php echo $combo5->getPrecio() ?>&pago=<?php echo $pago ?>&fecha=<?php echo $fecha ?>&cantidad=<?php echo $cantidad ?>&asignacion=<?php echo $asignacion ?>&asiento=<?php echo $asiento ?>&precio_pelicula=<?php echo $precioPelicula ?>"></a></h3>
										<span class="card__category" style="color: white; font-size: 14px">Descripción: <?php echo  $combo5->getDescripcion() ?></span>
										<span class="card__rate"><i class="icon ion-ios-pricetags"></i>$<?php echo $combo5->getPrecio() ?></span>
									</div>

								</div>


							</div>
							<!-- end card -->
						</div>
						<div class="item">
							<!-- card -->
							<div class="card card--big">
								<div class="card card--big">
									<div class="card__cover">
										<img src="../rscUser/img/productos/Combo6.jpg" alt="">
										<a href="respuesta.php?cod_confiteria=<?php echo $combo6->getCod_confiteria() ?>&precio_confiteria=<?php echo $combo6->getPrecio() ?>&pago=<?php echo $pago ?>&fecha=<?php echo $fecha ?>&cantidad=<?php echo $cantidad ?>&asignacion=<?php echo $asignacion ?>&asiento=<?php echo $asiento ?>&precio_pelicula=<?php echo $precioPelicula ?>" class="card__play">
											<i class="icon ion-ios-cart"></i>
										</a>
									</div>
									<div class="card__content">
										<h3 class="card__title"><a href="respuesta.php?cod_confiteria=<?php echo $combo6->getCod_confiteria() ?>&precio_confiteria=<?php echo $combo6->getPrecio() ?>&pago=<?php echo $pago ?>&fecha=<?php echo $fecha ?>&cantidad=<?php echo $cantidad ?>&asignacion=<?php echo $asignacion ?>&asiento=<?php echo $asiento ?>&precio_pelicula=<?php echo $precioPelicula ?>"></a></h3>
										<span class="card__category" style="color: white; font-size: 14px">Descripción: <?php echo  $combo6->getDescripcion() ?></span>
										<span class="card__rate"><i class="icon ion-ios-pricetags"></i>$<?php echo $combo6->getPrecio() ?></span>
									</div>

								</div>

							</div>
							
							<!-- end card -->
						</div>
					</div>
				</div>
			</div>
		</div>



	</section>
	<!-- page title -->
	<!-- JS -->
	<script src="../rscUser/js/jquery-3.3.1.min.js"></script>
	<script src="../rscUser/js/bootstrap.bundle.min.js"></script>
	<script src="../rscUser/js/owl.carousel.min.js"></script>
	<script src="../rscUser/js/jquery.mousewheel.min.js"></script>
	<script src="../rscUser/js/jquery.mCustomScrollbar.min.js"></script>
	<script src="../rscUser/js/wNumb.js"></script>
	<script src="../rscUser/js/nouislider.min.js"></script>
	<script src="../rscUser/js/plyr.min.js"></script>
	<script src="../rscUser/js/jquery.morelines.min.js"></script>
	<script src="../rscUser/js/photoswipe.min.js"></script>
	<script src="../rscUser/js/photoswipe-ui-default.min.js"></script>
	<script src="../rscUser/js/main.js"></script>
</body>

</html>