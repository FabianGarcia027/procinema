<?php
session_start();

require_once('../entidades/TicketPelicula.php');
require_once('../entidades/TicketConfiteria.php');
require_once('../entidades/TicketCompra.php');
require_once('../daos/TicketCompraDAO.php');
require_once('../daos/TicketPeliculaDAO.php');
require_once('../daos/TicketConfiteriaDAO.php');
require_once('../util/Conexion.php');
require_once('../daos/ClienteDAO.php');


$cliente = new ClienteDAO();
$conexion = new Conexion();
$con = $conexion->conectarBD();

    $operario= $_SESSION['codigo'];
  	$cod = (int)$_SESSION['cod_cli'];


 if (isset($_GET['cod_confiteria']) && isset($_GET['precio_confiteria']) && isset($_GET['pago']) && isset($_GET['fecha']) && isset($_GET['cantidad']) && isset($_GET['asignacion'])
 && isset($_GET['asiento']) && isset($_GET['precio_pelicula'])) {
    // echo ("Si confiteria");
    $codigo = (int) $_GET['cod_confiteria'];
    $precio = (int) $_GET['precio_confiteria'];
    $pago = (int)$_GET['pago'];
    $fecha = $_GET['fecha'];
    $cantidad = (int)$_GET['cantidad'];
    $asignacion2 = (int)$_GET['asignacion'];
    $asiento = $_GET['asiento'];
    $claves = preg_split("/[\s,]+/", $asiento);
    $precio_pelicula = (int)$_GET['precio_pelicula'];


    $ticketConfiteria = new TicketConfiteria(0, $codigo, $precio);
    $ticketConfiteriaDAO = new TicketConfiteriaDAO($con);
    $insertar = $ticketConfiteriaDAO->crearTicketConfiteria($ticketConfiteria);

    $ticketPelicula = new TicketPeliculaDAO($con);
    $cliente->agregarPuntosConfiteria($cod);
    $cliente->agregarPuntosPelicula($cod);
  #  $cliente->agregarPuntosConfiteria($cod);
    // echo ($asignacion2."->".$asiento."->".$precio_pelicula);
    // print_r($claves);
    $a = count($claves);
    $costo = 0;

    if(($precio_pelicula%11000) == 0){
        $costo = 11000;
    } else {
        $costo = 15000;
    }

    for ($i = 0; $i < $a; $i++) {
        $asignacion = new TicketPelicula(0, $asignacion2, $claves[$i], $costo);
        $insertarPelicula = $ticketPelicula->crearTicketCompra($asignacion);
    }

    $ultimo = $ticketPelicula->obtenerUltimo();
    $ultimo2 = $ticketConfiteriaDAO->obtenerUltimo();
    $noc = 0;
    $sic=0;

    foreach ($ultimo as $lista) {
        $noc = $lista->getCod_ticket_pelicula();
        $precioPelicula = $lista->getPrecio();
    }

    foreach ($ultimo2 as $lista2) {
        $sic = $lista2->getCod_ticket_confiteria();

    }

echo $operario;
    $precioTotal = $precio_pelicula + $precio;

    $compra = new TicketCompra(0, $operario, $pago, $cod, $sic, $noc, $fecha, $precioTotal);
    $ticketCompra = new TicketCompraDAO($con);
    $finalizar = $ticketCompra->crearTicketCompra($compra);
    $_SESSION['num']=1;
    header('Location: compraFinal.php');
 } else echo ("No confiteria");

if (
    isset($_POST['asignacion']) && isset($_POST['asiento']) && isset($_POST['precio']) && isset($_POST['f_pago']) && isset($_POST['cantidad'])
    && isset($_POST['boton']) && isset($_POST['ultimo']) && isset($_POST['fecha'])
) {

    $asignacion = (int) $_POST['asignacion'];
    $asiento = (int) $_POST['asiento'];
    $precio = (int) $_POST['precio'];
    $f_pago = (int) $_POST['f_pago'];
    $cantidad = (int) $_POST['cantidad'];
    $boton = (int) $_POST['boton'];
    $ultimoo = (int) $_POST['ultimo'];
    $precioTotal = $precio * $cantidad;
    $date = $_POST['fecha'];

    if ($boton == 2) {

        $asignacion = new TicketPelicula(0, $asignacion, $asiento, $precio);
        $ticketPelicula = new TicketPeliculaDAO($con);
        $insertar = $ticketPelicula->crearTicketCompra($asignacion);

        $ultimo = $ticketPelicula->obtenerUltimo();
        $noc;

        foreach ($ultimo as $lista) {
            $noc = $lista->getCod_ticket_pelicula();
        }


        if ($asiento == $ultimoo) {
            $compra = new TicketCompra(0, $operario, $f_pago, $cod, 79, $noc, $date, $precioTotal);
            $ticketCompra = new TicketCompraDAO($con);
            $finalizar = $ticketCompra->crearTicketCompra($compra);
            $cliente->agregarPuntosPelicula($cod);
            $_SESSION['num']=1;

        }
    }

}
