<!DOCTYPE html>
<?php

session_start();

 ?>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Font -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600%7CUbuntu:300,400,500,700" rel="stylesheet">

    <!-- CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <!-- <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"> -->
    <link rel="stylesheet" href="../rscUser/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="../rscUser/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="../rscUser/css/owl.carousel.min.css">
    <link rel="stylesheet" href="../rscUser/css/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="../rscUser/css/nouislider.min.css">
    <link rel="stylesheet" href="../rscUser/css/ionicons.min.css">
    <link rel="stylesheet" href="../rscUser/css/plyr.css">
    <link rel="stylesheet" href="../rscUser/css/photoswipe.css">
    <link rel="stylesheet" href="../rscUser/css/default-skin.css">
    <link rel="stylesheet" href="../rscUser/css/main.css">

    <!-- Favicons -->
    <link rel="icon" type="../rscUser/image/png" href="../rscUser/icon/logoC.png" sizes="32x32">
    <link rel="apple-touch-icon" href="icon/favicon-32x32.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../rscUser/icon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../rscUser/icon/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="144x144" href="../rscUser/icon/apple-touch-icon-144x144.png">

    <meta name="description" content="">
    <meta name="keywords" content="">
    <title>Procinema - Respuesta Pelicula</title>

</head>

<body class="body">

    <!-- header -->
    <header class="header">
        <div class="header__wrap">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="header__content">
                            <!-- header logo -->
                            <a href="index.php" class="header__logo">
                                <img src="../rscUser/img/logo.png" alt="">
                            </a>
                            <!-- end header logo -->

                            <ul class="header__nav">
                                <!-- dropdown -->
                                <li class="header__nav-item">
                                    <a href="index.php" class="header__nav-link">Inicio</a>
                                </li>
                                <!-- end dropdown -->
                            </ul>

                            <!-- header auth -->
                            <div class="header__auth">
                                <a href="../logout.php" class="header__sign-in">
                                    <i class="icon ion-ios-log-in"></i>
                                    <span>Cerrar sesión</span>
                                </a>
                            </div>
                            <!-- end header auth -->

                            <!-- header menu btn -->
                            <button class="header__btn" type="button">
                                <span></span>
                                <span></span>
                                <span></span>
                            </button>
                            <!-- end header menu btn -->
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- header search -->
        <form action="#" class="header__search">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="header__search-content">
                            <input type="text" placeholder="Search for a movie, TV Series that you are looking for">

                            <button type="button">search</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- end header search -->
    </header>
    <!-- end header -->

    <!-- page title -->
    <section class="section section--first section--bg" data-bg="img/section/section.jpg">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section__wrap">
                        <!-- section title -->
                        <h2 class="section__title">Respuesta de la Transacción</h2>
                        <!-- end section title -->

                        <!-- breadcrumb -->
                        <ul class="breadcrumb" style="background: transparent">
                            <li class="breadcrumb__item"><a href="index.php">Inicio</a></li>
                            <li class="breadcrumb__item breadcrumb__item--active">Transacción</li>
                        </ul>
                        <!-- end breadcrumb -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end page title -->

    <!-- faq -->
    <section class="section">
        <div class="container">
            <div class="row">

                <div class="container">
                    <div class="row" style="margin-top:20px">
                        <div class="col-lg-8 col-lg-offset-2 ">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr style="color: white; font-size: 18px;">
                                            <td>Referencia</td>
                                            <td id="referencia" style="width: 58%"></td>
                                        </tr>
                                        <tr style="color: white; font-size: 18px;">
                                            <td class="bold">Recibo</td>
                                            <td id="recibo"></td>
                                        </tr>
                                        <tr style="color: white; font-size: 18px;">
                                            <td class="bold">Fecha</td>
                                            <td id="fecha" class=""></td>
                                        </tr>
                                        <tr style="color: white; font-size: 18px;">
                                            <td>Respuesta</td>
                                            <td id="respuesta"></td>
                                        </tr>
                                        <tr style="color: white; font-size: 18px;">
                                            <td>Documento</td>
                                            <td id="documento"></td>
                                        </tr>
                                        <tr style="color: white; font-size: 18px;">
                                            <td class="bold">Banco</td>
                                            <td class="" id="banco">
                                        </tr>
                                        <tr style="color: white; font-size: 18px;">
                                            <td class="bold">Forma de pago</td>
                                            <td id="f_pago"></td>
                                        </tr>
                                        <tr style="color: white; font-size: 18px;">
                                            <td class="bold">Cantidoad de asientos</td>
                                            <td id="cantidad" name="cantidad"></td>
                                        </tr>
                                        <tr style="color: white; font-size: 18px;">
                                            <td class="bold">Descripción</td>
                                            <td id="descripcion"></td>
                                        </tr>
                                        <!-- <tr style="color: white; font-size: 18px;">
                                            <td class="bold">Asientos</td>
                                            <td id="asientos"></td>
                                        </tr>                                         -->
                                        <tr style="color: white; font-size: 18px;">
                                            <td class="bold">Total</td>
                                            <td class="" id="total">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table><br>
                                <button onclick="confiteriaa()" class="boton5" style="font-size: 14px; float: left" value="1">Confitería</button>
                                <button onclick="salir()" class="boton5" style="font-size: 14px; float: right">Salir</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <?php


    ?>

    <!-- end faq -->
    <footer>
        <div class="container">
            <div class="col-lg-8 col-lg-offset-2">
                <img src="https://369969691f476073508a-60bf0867add971908d4f26a64519c2aa.ssl.cf5.rackcdn.com/btns/epayco/pagos_procesados_por_epayco_260px.png" style="margin-top:10px; float:left">
                <img src="https://369969691f476073508a-60bf0867add971908d4f26a64519c2aa.ssl.cf5.rackcdn.com/btns/epayco/credibancologo.png" height="40px" style="margin-top:10px; float:right">
            </div>

        </div>
    </footer>
    <!-- end footer -->

    <!-- JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script>
    <script>
        function getQueryParam(param) {
            location.search.substr(1)
                .split("&")
                .some(function(item) { // returns first occurence and stops
                    return item.split("=")[0] == param && (param = item.split("=")[1])
                })
            return param
        }
        $(document).ready(function() {
            //llave publica del comercio

            //Referencia de payco que viene por url
            var ref_payco = getQueryParam('ref_payco');
            //Url Rest Metodo get, se pasa la llave y la ref_payco como paremetro
            var urlapp = "https://secure.epayco.co/validation/v1/reference/" + ref_payco;

            $.get(urlapp, function(response) {


                if (response.success) {

                    if (response.data.x_cod_response == 1) {
                        //Codigo personalizado
                        alert("Transaccion Aprobada");

                        console.log('transacción aceptada');
                    }
                    //Transaccion Rechazada
                    if (response.data.x_cod_response == 2) {
                        console.log('transacción rechazada');
                    }
                    //Transaccion Pendiente
                    if (response.data.x_cod_response == 3) {
                        console.log('transacción pendiente');
                    }
                    //Transaccion Fallida
                    if (response.data.x_cod_response == 4) {
                        console.log('transacción fallida');
                    }

                    $('#fecha').html(response.data.x_transaction_date);
                    $('#respuesta').html(response.data.x_response);
                    $('#referencia').text(response.data.x_id_invoice);
                    $('#documento').text(response.data.x_customer_document);
                    $('#recibo').text(response.data.x_transaction_id);
                    $('#f_pago').text(response.data.x_franchise);
                    $('#banco').text(response.data.x_bank_name);
                    $('#autorizacion').text(response.data.x_approval_code);
                    $('#descripcion').text(response.data.x_description);
                    // $('#asientos').text(response.data.x_extra1);
                    $('#cantidad').text(response.data.x_extra2);
                    $('#horario').text(response.data.x_extra3);
                    $('#total').text(response.data.x_amount + ' ' + response.data.x_currency_code);

                    asignacion = (response.data.x_extra3).toString();
                    asiento = ((response.data.x_extra1).split(","));
                    precio = (response.data.x_amount);
                    var fPago = (response.data.x_franchise);
                    cantidad = (response.data.x_extra2);
                    fecha = (response.data.x_transaction_date);

                    if ((precio % 11000) == 0) {
                        unidad = 11000;
                    } else {
                        unidad = 15000;
                    }

                    if ((fPago == "BA") || (fPago == "EF") || (fPago == "GA") || (fPago == "PR") || (fPago == "RS")) {
                        pago = 1;
                    } else pago = 2;

                    // confiteria(asiento, unidad, pago, cantidad, 0);
                    return asignacion, asiento, unidad, pago, cantidad, fecha, precio;

                } else {
                    alert("Error consultando la información");
                }
            });

        });

        var precio;
        var fecha;
        var asignacion = 0;
        var unidad = 0;
        var asiento = [];
        var pago = 0;
        var cantidad = 0;

        function confiteria(g, a, u, p, c, boton, fe, pe) {
            var b = 0;
            if (boton == 1) {
                b = 1;
            } else if (boton == 2) {
                b = 2;
            } else {
                b = 0;
            }
            g = asignacion;
            a = asiento;
            u = unidad;
            p = pago;
            c = cantidad;
            f = a[a.length - 1];
            fe = fecha;
            pe = precio;


            for (i in asiento) {
                a = asiento[i];

                $.ajax({
                    async: false,
                    data: {
                        "asignacion": g,
                        "asiento": a,
                        "precio": u,
                        "f_pago": p,
                        "cantidad": c,
                        "boton": b,
                        "ultimo": f,
                        "fecha": fe
                    },
                    url: "respuesta.php",
                    type: "post"
                });
            }

            if (b == 2) {
            window.location="index.php";
        } else {
            window.location="confiteria.php?f_pago="+p+"&fecha="+fe+"&cantidad="+c+"&asignacion="+g+"&asiento="+asiento+"&precio="+pe;
        }

        }

        function salir(g, a, u, p, c, boton, fe, pe) {
            boton = 2;
            confiteria(g, a, u, p, c, boton, fe, pe);
        }

        function confiteriaa(g, a, u, p, c, boton, fe, pe) {
            boton = 1;
            confiteria(g, a, u, p, c, boton, fe, pe);
        }
    </script>

    <script src="../rscUser/js/jquery-3.3.1.min.js"></script>
    <script src="../rscUser/js/bootstrap.bundle.min.js"></script>
    <script src="../rscUser/js/owl.carousel.min.js"></script>
    <script src="../rscUser/js/jquery.mousewheel.min.js"></script>
    <script src="../rscUser/js/jquery.mCustomScrollbar.min.js"></script>
    <script src="../rscUser/js/wNumb.js"></script>
    <script src="../rscUser/js/nouislider.min.js"></script>
    <script src="../rscUser/js/plyr.min.js"></script>
    <script src="../rscUser/js/jquery.morelines.min.js"></script>
    <script src="../rscUser/js/photoswipe.min.js"></script>
    <script src="../rscUser/js/photoswipe-ui-default.min.js"></script>
    <script src="../rscUser/js/main.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>

</html>
